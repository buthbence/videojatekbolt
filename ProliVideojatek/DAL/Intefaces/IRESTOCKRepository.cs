﻿// <copyright file="IRESTOCKRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// interface for RestockRepository
    /// </summary>
    public interface IRESTOCKRepository : IRepository<RESTOCK>, IRemovable
    {
    }
}
