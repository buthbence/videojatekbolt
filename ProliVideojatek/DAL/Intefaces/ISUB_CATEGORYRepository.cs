﻿// <copyright file="ISUB_CATEGORYRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for SubCategory repository.
    /// </summary>
    public interface ISUB_CATEGORYRepository : IRepository<SUB_CATEGORY>, IRemovable
    {
    }
}
