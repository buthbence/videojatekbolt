﻿// <copyright file="IRemovable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for repositories that are able to remove entities from a database.
    /// </summary>
    public interface IRemovable
    {
        /// <summary>
        /// Defines a method that removes an entity.
        /// </summary>
        /// <param name="id">entities id</param>
        void Remove(decimal id);
    }
}
