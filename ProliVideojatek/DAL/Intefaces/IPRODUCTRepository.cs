﻿// <copyright file="IPRODUCTRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for ProductRepository
    /// </summary>
    public interface IPRODUCTRepository : IRepository<PRODUCT>, IRemovable
    {
        /// <summary>
        /// Defines a method which changes a product's name.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="newName">new name</param>
        void ChangeName(decimal id, string newName);

        /// <summary>
        /// Defines a method which changes a product's price.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="newPrice">new price</param>
        void ChangePrice(decimal id, int newPrice);

        /// <summary>
        /// Defines a method which changes a product's subcategory.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="newSubcatId">new subcategories id</param>
        void ChangeSubCategory(decimal id, decimal newSubcatId);

        /// <summary>
        /// Defines a method which increases a products stock value.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="amount">amount</param>
        void IncreaseStock(decimal id, int amount);

        /// <summary>
        /// Defines a method which decreases a product's stock value.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="amount">amount</param>
        void DecreaseStock(decimal id, int amount);

        /// <summary>
        /// Defines a method which changes a product's discount value.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="percentage">percentage</param>
        void ChangeDiscount(decimal id, int percentage);
    }
}
