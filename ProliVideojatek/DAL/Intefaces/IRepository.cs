﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for EFRepository
    /// </summary>
    /// <typeparam name="TEntity">entity</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Defines a method that returns every entity from datacontext.
        /// </summary>
        /// <returns>entities</returns>
        IQueryable<TEntity> QueryAll();

        /// <summary>
        /// Defines a method that insrets a new enitity to datacontex.
        /// </summary>
        /// <param name="newEntity">new entity</param>
        void Insert(TEntity newEntity);
    }
}
