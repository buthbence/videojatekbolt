﻿// <copyright file="IEMPLOYEERepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for EmployeeRepository
    /// </summary>
    public interface IEMPLOYEERepository : IRepository<EMPLOYEE>, IRemovable
    {
        /// <summary>
        /// Defines a method which changes an employee's name.
        /// </summary>
        /// <param name="id">employee id</param>
        /// <param name="newName">new name</param>
        void ChangeName(decimal id, string newName);

        /// <summary>
        /// Defines a method which changes an employee's username.
        /// </summary>
        /// <param name="id">employee id</param>
        /// <param name="newUserName">new user name</param>
        void ChangeUserName(decimal id, string newUserName);

        /// <summary>
        /// Defines a method which changes an employee's password.
        /// </summary>
        /// <param name="id">employee id</param>
        /// <param name="passwd">new password</param>
        void ChangePassword(decimal id, string passwd);

        /// <summary>
        /// Defines a method which changes an employee's boss status.
        /// </summary>
        /// <param name="id">employee id</param>
        void ChangeBossStatus(decimal id);

        /// <summary>
        /// Defines a method which changes an employee's reset password status.
        /// </summary>
        /// <param name="id">employee id</param>
        void ChangeResetPasswordStatus(decimal id);
    }
}
