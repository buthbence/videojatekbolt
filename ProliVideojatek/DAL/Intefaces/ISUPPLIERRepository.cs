﻿// <copyright file="ISUPPLIERRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for SupplierRepository.
    /// </summary>
    public interface ISUPPLIERRepository : IRepository<SUPPLIER>, IRemovable
    {
        /// <summary>
        /// Defines a method that changes a supplier's name.
        /// </summary>
        /// <param name="id">supplier name</param>
        /// <param name="newName">new name</param>
        void ChangeName(decimal id, string newName);

        /// <summary>
        /// Defines a method that changes a suppliers email.
        /// </summary>
        /// <param name="id">suppllier id</param>
        /// <param name="newEmail">new email</param>
        void ChangeEmail(decimal id, string newEmail);

        /// <summary>
        /// Defines a method that changes a suppliers phone number.
        /// </summary>
        /// <param name="id">Supplier id</param>
        /// <param name="newPhone">new phone number</param>
        void ChangePhone(decimal id, string newPhone);
    }
}
