﻿// <copyright file="ICUSTOMERRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for CustomerRepository
    /// </summary>
    public interface ICUSTOMERRepository : IRepository<CUSTOMER>, IRemovable
    {
        /// <summary>
        /// Defines a method which changes a customer's name.
        /// </summary>
        /// <param name="id">customer id</param>
        /// <param name="newName">new name</param>
        void ChangeName(decimal id, string newName);

        /// <summary>
        /// Defines a method which changes a customer's address.
        /// </summary>
        /// <param name="id">customer id</param>
        /// <param name="newAddress">new address</param>
        void ChangeAddres(decimal id, string newAddress);

        /// <summary>
        /// Defines a method which changes a customer's email.
        /// </summary>
        /// <param name="id">customer id</param>
        /// <param name="newEmail">new email</param>
        void ChangeEmail(decimal id, string newEmail);

        /// <summary>
        /// Defines a method which changes a customer's phone number.
        /// </summary>
        /// <param name="id">customer id</param>
        /// <param name="newPhone">new phone number</param>
        void ChangePhone(decimal id, string newPhone);

        /// <summary>
        /// Defines a method which changes a customer's loyalty card status.
        /// </summary>
        /// <param name="id">customer id</param>
        void ChangeLoyaltyCardStatus(decimal id);
    }
}
