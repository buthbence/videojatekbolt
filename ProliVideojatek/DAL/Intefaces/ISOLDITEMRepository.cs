﻿// <copyright file="ISOLDITEMRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for SolditemRepository.
    /// </summary>
    public interface ISOLDITEMRepository : IRepository<SOLDITEM>, IRemovable
    {
    }
}
