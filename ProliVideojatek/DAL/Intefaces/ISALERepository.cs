﻿// <copyright file="ISALERepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Intefaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for SaleRepository.
    /// </summary>
    public interface ISALERepository : IRepository<SALE>, IRemovable
    {
        /// <summary>
        /// Defines a method that changes a sale's reservation status.
        /// </summary>
        /// <param name="id">sale id</param>
        void ChangeReservationStatus(decimal id);
    }
}
