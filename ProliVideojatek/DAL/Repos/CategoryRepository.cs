﻿// <copyright file="CategoryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// Category repository.
    /// </summary>
    public class CategoryRepository : EFRepository<CATEGORY>, ICATEGORYRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryRepository"/> class.
        /// </summary>
        /// <param name="entities">db context</param>
        public CategoryRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Removes a category from Data context.
        /// </summary>
        /// <param name="id">Category id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<CATEGORY>().Remove(this.Entities.Set<CATEGORY>().Single(x => x.CAT_ID == id));
            this.Entities.SaveChanges();
        }
    }
}
