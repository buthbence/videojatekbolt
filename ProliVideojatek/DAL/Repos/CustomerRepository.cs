﻿// <copyright file="CustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// CustomerRepository.
    /// </summary>
    public class CustomerRepository : EFRepository<CUSTOMER>, ICUSTOMERRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public CustomerRepository(SHOPDatabaseEntities entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Chanhes a customer's address.
        /// </summary>
        /// <param name="id">customer id</param>
        /// <param name="newAddress">new address</param>
        public void ChangeAddres(decimal id, string newAddress)
        {
            this.Entities.Set<CUSTOMER>().Single(x => x.CID == id).ADDR = newAddress;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes a customer's email.
        /// </summary>
        /// <param name="id">customer id</param>
        /// <param name="newEmail">new email</param>
        public void ChangeEmail(decimal id, string newEmail)
        {
            this.Entities.Set<CUSTOMER>().Single(x => x.CID == id).EMAIL = newEmail;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes a customer's loyalty card status.
        /// </summary>
        /// <param name="id">customer id</param>
        public void ChangeLoyaltyCardStatus(decimal id)
        {
            var result = this.Entities.Set<CUSTOMER>().Single(x => x.CID == id);
            if (result.LCARD.Equals(true))
            {
                this.Entities.Set<CUSTOMER>().Single(x => x.CID == id).LCARD = false;
            }
            else if (result.LCARD.Equals(false))
            {
                this.Entities.Set<CUSTOMER>().Single(x => x.CID == id).LCARD = true;
            }

            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes a customer's name.
        /// </summary>
        /// <param name="id">customer id</param>
        /// <param name="newName">new name</param>
        public void ChangeName(decimal id, string newName)
        {
            this.Entities.Set<CUSTOMER>().Single(x => x.CID == id).CNAME = newName;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes a customer's phone number.
        /// </summary>
        /// <param name="id">customer id</param>
        /// <param name="newPhone">new phone number</param>
        public void ChangePhone(decimal id, string newPhone)
        {
            this.Entities.Set<CUSTOMER>().Single(x => x.CID == id).PHONE = newPhone;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Removes a customer from dbcontext.
        /// </summary>
        /// <param name="id">customer id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<CUSTOMER>().Single(x => x.CID == id).ACTIVE = false;
            this.Entities.SaveChanges();
        }
    }
}
