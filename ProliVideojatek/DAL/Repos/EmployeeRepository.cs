﻿// <copyright file="EmployeeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// EmployeeRepository.
    /// </summary>
    public class EmployeeRepository : EFRepository<EMPLOYEE>, IEMPLOYEERepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public EmployeeRepository(SHOPDatabaseEntities entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Changes an employee's boss status.
        /// </summary>
        /// <param name="id">employee id</param>
        public void ChangeBossStatus(decimal id)
        {
            bool status = (bool)this.Entities.Set<EMPLOYEE>().Single(x => x.EID == id).BOSS;
            this.Entities.Set<EMPLOYEE>().Single(x => x.EID == id).BOSS = !status;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes an employee's name.
        /// </summary>
        /// <param name="id">employee id</param>
        /// <param name="newName">new name</param>
        public void ChangeName(decimal id, string newName)
        {
            this.Entities.Set<EMPLOYEE>().Single(x => x.EID == id).ENAME = newName;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes an employee's password.
        /// </summary>
        /// <param name="id">employee id</param>
        /// <param name="passwd">new password</param>
        public void ChangePassword(decimal id, string passwd)
        {
            this.Entities.Set<EMPLOYEE>().Single(x => x.EID == id).PWORD = passwd;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes an employee's username.
        /// </summary>
        /// <param name="id">employee id</param>
        /// <param name="newUserName">new username</param>
        public void ChangeUserName(decimal id, string newUserName)
        {
            this.Entities.Set<EMPLOYEE>().Single(x => x.EID == id).UNAME = newUserName;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes an employee's reset password status.
        /// </summary>
        /// <param name="id">employee id</param>
        public void ChangeResetPasswordStatus(decimal id)
        {
            this.Entities.Set<EMPLOYEE>().Single(x => x.EID == id).PASSRESET = !this.Entities.Set<EMPLOYEE>().Single(x => x.EID == id).PASSRESET;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Removes an employee.
        /// </summary>
        /// <param name="id">employee id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<EMPLOYEE>().Single(x => x.EID == id).ACTIVE = false;
            this.Entities.SaveChanges();
        }
    }
}
