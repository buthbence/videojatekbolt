﻿// <copyright file="UniversalRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// UniversalRepository used in BLL.
    /// </summary>
    public static class UniversalRepository
    {
        private static SHOPDatabaseEntities shopDatabaseEntities = new SHOPDatabaseEntities();

        private static CategoryRepository categoryRepository = new CategoryRepository(shopDatabaseEntities);

        private static CustomerRepository customerRepository = new CustomerRepository(shopDatabaseEntities);

        private static EmployeeRepository employeeRepository = new EmployeeRepository(shopDatabaseEntities);

        private static ProductRepository productRepository = new ProductRepository(shopDatabaseEntities);

        private static RestockRepository restockRepository = new RestockRepository(shopDatabaseEntities);

        private static SaleRepository saleRepository = new SaleRepository(shopDatabaseEntities);

        private static SolditemRepository soldItemRepository = new SolditemRepository(shopDatabaseEntities);

        private static SubCategoryRepository subCategoryRepository = new SubCategoryRepository(shopDatabaseEntities);

        private static SupplierRepository supplierRepository = new SupplierRepository(shopDatabaseEntities);

        private static SupProdRepository supProdRepository = new SupProdRepository(shopDatabaseEntities);

        /// <summary>
        /// Gets a CategoryRepository.
        /// </summary>
        public static CategoryRepository CategoryRepository
        {
            get
            {
                return categoryRepository;
            }
        }

        /// <summary>
        /// Gets a CustomerRepository.
        /// </summary>
        public static CustomerRepository CustomerRepository
        {
            get
            {
                return customerRepository;
            }
        }

        /// <summary>
        /// Gets an EmployeeRepository.
        /// </summary>
        public static EmployeeRepository EmployeeRepository
        {
            get
            {
                return employeeRepository;
            }
        }

        /// <summary>
        /// Gets a ProductRepository.
        /// </summary>
        public static ProductRepository ProductRepository
        {
            get
            {
                return productRepository;
            }
        }

        /// <summary>
        /// Gets a RestockRepository.
        /// </summary>
        public static RestockRepository RestockRepository
        {
            get
            {
                return restockRepository;
            }
        }

        /// <summary>
        /// Gets a SaleRepository.
        /// </summary>
        public static SaleRepository SaleRepository
        {
            get
            {
                return saleRepository;
            }
        }

        /// <summary>
        /// Gets a SolditemRepository.
        /// </summary>
        public static SolditemRepository SoldItemRepository
        {
            get
            {
                return soldItemRepository;
            }
        }

        /// <summary>
        /// Gets a SubCategroyRepository.
        /// </summary>
        public static SubCategoryRepository SubCategoryRepository
        {
            get
            {
                return subCategoryRepository;
            }
        }

        /// <summary>
        /// Gets a SupplierRepository.
        /// </summary>
        public static SupplierRepository SupplierRepository
        {
            get
            {
                return supplierRepository;
            }
        }

        /// <summary>
        /// Gets a SupProdRepository.
        /// </summary>
        public static SupProdRepository SupProdRepository
        {
            get
            {
                return supProdRepository;
            }
        }
    }
}
