﻿// <copyright file="SupProdRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// SupProdRepository.
    /// </summary>
    public class SupProdRepository : EFRepository<SUPPROD>, ISUPPRODRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SupProdRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public SupProdRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Removes a SupProd object from DbContext.
        /// </summary>
        /// <param name="id">supprod id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<SUPPROD>().Remove(this.Entities.Set<SUPPROD>().Single(x => x.SPID == id));
            this.Entities.SaveChanges();
        }
    }
}
