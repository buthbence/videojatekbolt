﻿// <copyright file="SupplierRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// SupplierRepository.
    /// </summary>
    public class SupplierRepository : EFRepository<SUPPLIER>, ISUPPLIERRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SupplierRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public SupplierRepository(SHOPDatabaseEntities entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Changes a supplier's email.
        /// </summary>
        /// <param name="id">supplier id</param>
        /// <param name="newEmail">new email</param>
        public void ChangeEmail(decimal id, string newEmail)
        {
            this.Entities.Set<SUPPLIER>().Single(x => x.SID == id).SEMAIL = newEmail;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes a supplier's name.
        /// </summary>
        /// <param name="id">supplier id</param>
        /// <param name="newName">new name</param>
        public void ChangeName(decimal id, string newName)
        {
            this.Entities.Set<SUPPLIER>().Single(x => x.SID == id).SNAME = newName;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes a supplier's phone number.
        /// </summary>
        /// <param name="id">supplier id</param>
        /// <param name="newPhone">new phone number</param>
        public void ChangePhone(decimal id, string newPhone)
        {
            this.Entities.Set<SUPPLIER>().Single(x => x.SID == id).SPHONE = newPhone;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Removes a supplier from DbContext.
        /// </summary>
        /// <param name="id">supplier id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<SUPPLIER>().Single(x => x.SID == id).ACTIVE = false;
            this.Entities.SaveChanges();
        }
    }
}
