﻿// <copyright file="SaleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// SaleRepository.
    /// </summary>
    public class SaleRepository : EFRepository<SALE>, ISALERepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SaleRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public SaleRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Changes a sale's reservation status.
        /// </summary>
        /// <param name="id">sale id</param>
        public void ChangeReservationStatus(decimal id)
        {
            bool status = this.Entities.Set<SALE>().Single(x => x.SALID == id).RESERVED;
            this.Entities.Set<SALE>().Single(x => x.SALID == id).RESERVED = !status;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Removes a sale object from DbContext.
        /// </summary>
        /// <param name="id">sale id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<SALE>().Remove(this.Entities.Set<SALE>().Single(x => x.SALID == id));
            this.Entities.SaveChanges();
        }
    }
}
