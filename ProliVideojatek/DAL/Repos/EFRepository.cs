﻿// <copyright file="EFRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Intefaces;

    /// <summary>
    /// Entity Framework repository.
    /// </summary>
    /// <typeparam name="TEntity">renitity</typeparam>
    public class EFRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private DbContext entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="EFRepository{TEntity}"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public EFRepository(DbContext entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Gets entities.
        /// </summary>
        protected DbContext Entities
        {
            get
            {
                return this.entities;
            }
        }

        /// <summary>
        /// Gets all entity from DbContext.
        /// </summary>
        /// <returns>all entity</returns>
        public IQueryable<TEntity> QueryAll()
        {
            return this.Entities.Set<TEntity>();
        }

        /// <summary>
        /// Inserts a new entity to DbContext.
        /// </summary>
        /// <param name="newEntity">new entity</param>
        public void Insert(TEntity newEntity)
        {
            this.Entities.Set<TEntity>().Add(newEntity);
            this.Entities.SaveChanges();
        }
    }
}
