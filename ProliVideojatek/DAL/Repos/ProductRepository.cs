﻿// <copyright file="ProductRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// ProductRepository.
    /// </summary>
    public class ProductRepository : EFRepository<PRODUCT>, IPRODUCTRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public ProductRepository(SHOPDatabaseEntities entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Changes a produtc's name.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="newName">new name</param>
        public void ChangeName(decimal id, string newName)
        {
            this.Entities.Set<PRODUCT>().Single(x => x.PID == id).PNAME = newName;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes a product's price.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="newPrice">new price</param>
        public void ChangePrice(decimal id, int newPrice)
        {
            this.Entities.Set<PRODUCT>().Single(x => x.PID == id).PRICE = newPrice;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Changes a products subcategory.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="newSubcatId">new subcategory id</param>
        public void ChangeSubCategory(decimal id, decimal newSubcatId)
        {
            this.Entities.Set<PRODUCT>().Single(x => x.PID == id).PSUBCAT = newSubcatId;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Decreases a product's stock value.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="amount">amount</param>
        public void DecreaseStock(decimal id, int amount)
        {
            this.Entities.Set<PRODUCT>().Single(x => x.PID == id).STOCK -= amount;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Increases a product's stock value.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="amount">amount</param>
        public void IncreaseStock(decimal id, int amount)
        {
            this.Entities.Set<PRODUCT>().Single(x => x.PID == id).STOCK += amount;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Removes a product from DbContext.
        /// </summary>
        /// <param name="id">product id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<PRODUCT>().Single(x => x.PID == id).ACTIVE = false;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Change a product's discount value.
        /// </summary>
        /// <param name="id">product id</param>
        /// <param name="percentage">precantage of discount</param>
        public void ChangeDiscount(decimal id, int percentage)
        {
            this.Entities.Set<PRODUCT>().Single(x => x.PID == id).DISCOUNT = percentage;
            this.Entities.SaveChanges();
        }
    }
}
