﻿// <copyright file="SolditemRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;
    using DAL.Repos;

    /// <summary>
    /// SolditemRepository.
    /// </summary>
    public class SolditemRepository : EFRepository<SOLDITEM>, ISOLDITEMRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SolditemRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public SolditemRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Removes a solditem from DbContext.
        /// </summary>
        /// <param name="id">solditem id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<SOLDITEM>().Remove(this.Entities.Set<SOLDITEM>().Single(x => x.SOLDID == id));
            this.Entities.SaveChanges();
        }
    }
}
