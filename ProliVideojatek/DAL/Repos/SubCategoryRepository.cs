﻿// <copyright file="SubCategoryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// SubCategoryRepository.
    /// </summary>
    public class SubCategoryRepository : EFRepository<SUB_CATEGORY>, ISUB_CATEGORYRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubCategoryRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public SubCategoryRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Removes a subcategory from DbContext.
        /// </summary>
        /// <param name="id">subcategory id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<SUB_CATEGORY>().Remove(this.Entities.Set<SUB_CATEGORY>().Single(x => x.SUB_CAT_ID == id));
            this.Entities.SaveChanges();
        }
    }
}
