﻿// <copyright file="RestockRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DAL.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL.Intefaces;

    /// <summary>
    /// RestockRepository.
    /// </summary>
    public class RestockRepository : EFRepository<RESTOCK>, IRESTOCKRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestockRepository"/> class.
        /// </summary>
        /// <param name="entities">entities</param>
        public RestockRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Removes a restock record from DbContext.
        /// </summary>
        /// <param name="id">restock id</param>
        public void Remove(decimal id)
        {
            this.Entities.Set<RESTOCK>().Remove(this.Entities.Set<RESTOCK>().Single(x => x.RID == id));
            this.Entities.SaveChanges();
        }
    }
}
