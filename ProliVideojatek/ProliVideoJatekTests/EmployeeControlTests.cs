﻿// <copyright file="EmployeeControlTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideoJatekTests
{
    using System.Collections.Generic;
    using System.Linq;
    using Bll;
    using DAL;
    using DAL.Repos;
    using NUnit.Framework;

    /// <summary>
    /// Class of EmployeeControl tests
    /// </summary>
    [TestFixture]
    public class EmployeeControlTests
    {
        /// <summary>
        /// Test the Illegal characters when illegal name is given
        /// </summary>
        [Test]
        public void EmployeeControl_Should_RemoveIllegalCharacters_When_IllegalNameIsGiven()
        {
            // Arrange
            string illegal = "Ádám";
            string legal = "Adam";

            // Act
            string result = EmployeeControl.CreateUsernameFromName(illegal);

            // Assert
            StringAssert.AreEqualIgnoringCase(result, legal);
        }

        /// <summary>
        /// Test the new user method
        /// </summary>
        [Test]
        public void IsNewUser_Should_GiveTrue_When_NewUsernameInput()
        {
            // ARRANGE
            EmployeeControl employeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository);
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            string newUserName = "Teszt";

            // ACT
            bool result = employeeControl.IsNewUser(newUserName);

            // ASSERT
            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Test the AddNewEmployee method
        /// </summary>
        [Test]
        public void AddNewEmployee_Should_CorrectCount_When_addNewMember()
        {
            // ARRANGE
            EmployeeControl employeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository);
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();

            // ACT
            int beforeAdd = entities.EMPLOYEE.Count();
            employeeControl.AddNewEmployee(2, "teszt", "tesztes", "asdasd", false);

            // ASSERT
            Assert.That(beforeAdd, Is.EqualTo(entities.EMPLOYEE.Count() - 1));
        }

        /// <summary>
        /// Test the EditEmployee method
        /// </summary>
        [Test]
        public void EditEmployee_Should_EditName_When_NameIsChanged()
        {
            // ARRANGE
            EmployeeControl employeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository);
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            string newName = "Teszt_2";

            // ACT
            employeeControl.EditEmployee(2, newName, "tesztes", "asdasd", false, false);
            var eMPLOYEE = entities.EMPLOYEE.Single(x => x.EID == 2);

            // ASSERT
            Assert.That(eMPLOYEE.ENAME, Is.EqualTo(newName));
        }

        /// <summary>
        /// Test the DeleteEmployee method
        /// </summary>
        [Test]
        public void DeleteEmployee_Should_SetActiveToFalse_When_Remove()
        {
            // ARRANGE
            EmployeeControl employeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository);
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            decimal id = 2;

            // ACT
            employeeControl.DeleteEmployee(id);
            var eMPLOYEEStatues = entities.EMPLOYEE.Single(x => x.EID == 2).ACTIVE;

            // ASSERT
            Assert.That(eMPLOYEEStatues, Is.EqualTo(false));
        }

        /// <summary>
        /// Test the GetEmployee method
        /// </summary>
        [Test]
        public void GetEmployees_Should_CountCorrectle_When_GenerateList()
        {
            // ARRANGE
            EmployeeControl employeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository);
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            string search = "admin";

            // ACT
            List<EMPLOYEE> list = employeeControl.GetEmployees(search);

            // ASSERT
            Assert.That(list.Count(), Is.EqualTo(1));
        }

        /// <summary>
        /// Test the GetNewId method
        /// </summary>
        [Test]
        public void GetNewId_Should_GiveCorrectID_When_GenerateNew()
        {
            // ARRANGE
            EmployeeControl employeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository);
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();

            // ACT
            decimal result = employeeControl.GetNewId;
            decimal expected = entities.EMPLOYEE.Max(x => x.EID) + 1;

            // ASSERT
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}
