﻿// <copyright file="CustomerControlTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideoJatekTests
{
    using System.Collections.Generic;
    using System.Linq;
    using Bll;
    using DAL;
    using NUnit.Framework;

    /// <summary>
    /// Test class for Customer tests
    /// </summary>
    [TestFixture]
    internal class CustomerControlTest
    {
        /// <summary>
        /// Active Customers test
        /// </summary>
        [Test]
        public void ActiveCustomers_Should_ReturnCorrectList_When_GetActiveCustomers()
        {
            // ARRANGE
            CustomerControl customerControl = new CustomerControl();
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();

            // ACT
            List<CUSTOMER> list = customerControl.ActiveCustomers;
            var result = entities.CUSTOMER.Select(x => x.ACTIVE == true).Count();

            // ASSERT
            Assert.That(list.Count(), Is.EqualTo(result));
        }

        /// <summary>
        /// Test the Customer add method
        /// </summary>
        [Test]
        public void AddCustomer_Should_IncreaseCustomerCount_When_NewCustomerIsAdd()
        {
            // ARRANGE
            CustomerControl customerControl = new CustomerControl();
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            var beforeCount = entities.CUSTOMER.Count();
            CUSTOMER cUSTOMER = new CUSTOMER()
            {
                ADDR = "addr",
                CNAME = "cname",
                EMAIL = "mail",
                LCARD = false,
                PHONE = "1111111",
            };

            // ACT
            customerControl.AddCustomer(cUSTOMER);

            // ASSERT
            Assert.That(beforeCount, Is.EqualTo(entities.CUSTOMER.Count() - 1));
        }

        /// <summary>
        /// Test the remove Customer method
        /// </summary>
        [Test]
        public void RemoveCustomer_Should_SetCustomerActivetoFalse_When_CustomerIsRemoved()
        {
            // ARRANGE
            CustomerControl customerControl = new CustomerControl();
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            CUSTOMER cUSTOMER = new CUSTOMER()
            {
                ADDR = "addr",
                CID = 3,
                CNAME = "cname",
                EMAIL = "mail",
                LCARD = false,
                PHONE = "1111111",
            };

            // ACT
            customerControl.RemoveCustomer(cUSTOMER);
            var temp = entities.CUSTOMER.SingleOrDefault(x => x.CID == 3);

            // ASSERT
            Assert.That(temp.ACTIVE, Is.False);
        }

        /// <summary>
        /// Test the customer data change
        /// </summary>
        [Test]
        public void ChangeCustomerData_Should_ChangeAddress_When_CustomerAddressIsChange()
        {
            // ARRANGE
            CustomerControl customerControl = new CustomerControl();
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            string newAddr = "rdda";
            CUSTOMER cUSTOMER = new CUSTOMER()
            {
                ADDR = newAddr,
                CID = 3,
                CNAME = "cname",
                EMAIL = "mail",
                LCARD = false,
                PHONE = "1111111",
            };

            // ACT
            customerControl.ChangeCustomerData(cUSTOMER);
            var temp = entities.CUSTOMER.Where(x => x.CID == 3).Select(x => x.ADDR);
            string resultAddr = default(string);
            foreach (var akt in temp)
            {
                resultAddr = akt;
            }

            // ASSERT
            Assert.That(resultAddr, Is.EqualTo(newAddr));
        }

        /// <summary>
        /// Test the Customer filter
        /// </summary>
        [Test]
        public void FilterCustomers_Should_ListCountCorrect_When_Search()
        {
            // ARRANGE
            CustomerControl customerControl = new CustomerControl();
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            string search = "derek";

            // ACT
            List<CUSTOMER> result = customerControl.FilterCustomers(search);

            // ASSERT
            Assert.That(result.Count, Is.EqualTo(1));
        }

        /// <summary>
        /// Test the sold item by customer ID.
        /// </summary>
        [Test]
        public void GetSoldItemsByCustomerId_Should_ListCountCorrect_When_GetItems()
        {
            // ARRANGE
            CustomerControl customerControl = new CustomerControl();
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();
            decimal id = 1;

            // ACT
            List<SOLDITEM> result = customerControl.GetSoldItemsByCustomerId(id);
            var exceptedTemp = entities.SOLDITEM.Where(x => x.SALE.CID == id && x.SALE.RESERVED == false).OrderBy(x => x.SALE.DATE);
            List<SOLDITEM> excepted = new List<SOLDITEM>();
            foreach (var akt in exceptedTemp)
            {
                excepted.Add(akt);
            }

            // ASSERT
            Assert.That(result.Count, Is.EqualTo(excepted.Count));
        }
    }
}
