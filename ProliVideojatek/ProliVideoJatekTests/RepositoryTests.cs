﻿// <copyright file="RepositoryTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideoJatekTests
{
    using System.Collections.Generic;
    using System.Linq;
    using Bll;
    using DAL;
    using DAL.Intefaces;
    using NUnit.Framework;

    /// <summary>
    /// Class ot he Repository tests
    /// </summary>
    [TestFixture]
    internal class RepositoryTests
    {
        /// <summary>
        /// Test the MinimumPrice
        /// </summary>
        [Test]
        public void ProductControlClass_Should_ReturnTheProductWithTheSmallestPrice_When_MinimumPriceIsCalled()
        {
            List<PRODUCT> prod = new List<PRODUCT>();
            prod.Add(new PRODUCT() { PRICE = 100 });
            prod.Add(new PRODUCT() { PRICE = 200 });
            prod.Add(new PRODUCT() { PRICE = 300 });
            Moq.Mock<IPRODUCTRepository> mockRepo = new Moq.Mock<IPRODUCTRepository>();
            mockRepo.Setup(repo => repo.QueryAll()).Returns(() => prod.AsQueryable());

            Moq.Mock<ProductControl> productMoq = new Moq.Mock<ProductControl>(mockRepo.Object, null, null, null, null, null);
            int min = productMoq.Object.MinimumPrice;

            Assert.AreEqual(100, min);
        }

        /// <summary>
        /// Test the MaximumPrice
        /// </summary>
        [Test]
        public void ProductControlClass_Should_ReturnTheProductWithTheBiggestPrice_When_MaximumPriceIsCalled()
        {
            List<PRODUCT> prod1 = new List<PRODUCT>();
            prod1.Add(new PRODUCT() { PRICE = 300 });
            prod1.Add(new PRODUCT() { PRICE = 200 });
            prod1.Add(new PRODUCT() { PRICE = 400 });
            Moq.Mock<IPRODUCTRepository> mockRepo1 = new Moq.Mock<IPRODUCTRepository>();
            mockRepo1.Setup(repo => repo.QueryAll()).Returns(() => prod1.AsQueryable());

            Moq.Mock<ProductControl> productMoq1 = new Moq.Mock<ProductControl>(mockRepo1.Object, null, null, null, null, null);
            int max = productMoq1.Object.MaximumPrice;

            Assert.AreEqual(400, max);
        }
    }
}
