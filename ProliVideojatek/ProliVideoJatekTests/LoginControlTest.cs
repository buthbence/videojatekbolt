﻿// <copyright file="LoginControlTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideoJatekTests
{
    using Bll;
    using DAL;
    using DAL.Repos;
    using NUnit.Framework;

    /// <summary>
    /// Class of the LoginControl test
    /// </summary>
    [TestFixture]
    internal class LoginControlTest
    {
        /// <summary>
        /// Test the Authenticate method
        /// </summary>
        [Test]
        public void Authenticate_Should_ReturnCorrectOutput_When_InputUnameAndPW()
        {
            // ARRANGE
            LoginControl loginControl = new LoginControl(UniversalRepository.EmployeeRepository);
            string uname = "admin";
            string pw = "admin";
            int expected = 1;

            // ACT
            EMPLOYEE result = loginControl.Authenticate(uname, pw);

            // ASSERT
            Assert.That(result.EID, Is.EqualTo(expected));
        }

        /// <summary>
        /// Test the IsfirstTime method.
        /// </summary>
        [Test]
        public void IsFirstTime_Should_ReturnFalse_When_AdminIsInput()
        {
            // ARRANGE
            LoginControl loginControl = new LoginControl(UniversalRepository.EmployeeRepository);
            string uname = "admin";
            string pw = "admin";
            EMPLOYEE eMPLOYEE = loginControl.Authenticate(uname, pw);

            // ACT
            bool result = loginControl.IsFirstTime(eMPLOYEE);

            // ASSERT
            Assert.That(result, Is.EqualTo(false));
        }

        /// <summary>
        /// Test the UpdatePassword method
        /// </summary>
        [Test]
        public void UpdatePassword_Should_ReturnTrue_When_AdminIsInput()
        {
            // ARRANGE
            LoginControl loginControl = new LoginControl(UniversalRepository.EmployeeRepository);

            string uname = "admin";
            string pw = "admin";
            string newPW = "admin_1";
            string newPWConfirm = "admin_1";
            EMPLOYEE eMPLOYEE = loginControl.Authenticate(uname, pw);

            // ACT
            bool result = loginControl.UpdatePassword(eMPLOYEE, newPW, newPWConfirm);

            // ASSERT
            Assert.That(result, Is.EqualTo(true));
        }
    }
}
