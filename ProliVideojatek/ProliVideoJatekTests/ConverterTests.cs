﻿// <copyright file="ConverterTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideoJatekTests
{
    using System.Globalization;
    using DAL;
    using Moq;
    using NUnit.Framework;
    using ProliVideojatek;

    /// <summary>
    /// Test for the Converters
    /// </summary>
    [TestFixture]
    internal class ConverterTests
    {
        /// <summary>
        /// Test for the SupProdBoolConverter
        /// </summary>
        [Test]
        public void SupProdBoolConverter_Should_ReturnFalse_When_SupProdDoesNotSupplyTheProduct()
        {
            // Arrange
            Mock<PRODUCT> prodMoq = new Mock<PRODUCT>();

            // Act
            SupProdBoolConverter conv = new SupProdBoolConverter();
            bool result = (bool)conv.Convert(prodMoq.Object.PID, null, null, CultureInfo.InvariantCulture);

            // Assert
            Assert.IsFalse(result);
        }
    }
}
