//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// Category class.
    /// </summary>
    public partial class CATEGORY
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CATEGORY"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CATEGORY()
        {
            this.SUB_CATEGORY = new HashSet<SUB_CATEGORY>();
        }
        
        /// <summary>
        /// Gets or sets a category's id.
        /// </summary>
        public decimal CAT_ID { get; set; }

        /// <summary>
        /// Gets or sets a category's id.
        /// </summary>
        public string CAT_NAME { get; set; }

        /// <summary>
        /// Gets or sets a Subcategory collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SUB_CATEGORY> SUB_CATEGORY { get; set; }
    }
}
