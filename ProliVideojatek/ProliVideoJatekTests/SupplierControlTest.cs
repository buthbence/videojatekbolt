﻿// <copyright file="SupplierControlTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideoJatekTests
{
    using System.Collections.Generic;
    using System.Linq;
    using Bll;
    using DAL;
    using NUnit.Framework;

    /// <summary>
    /// Class of the Supplier Test
    /// </summary>
    [TestFixture]
    internal class SupplierControlTest
    {
        /// <summary>
        /// Getsupplier test
        /// </summary>
        [Test]
        public void GetSuppliers_Should_CountCorrect_When_GetActiveSuppliers()
        {
            // ARRANGE
            SupplierControl supplierControl = new SupplierControl();
            SHOPDatabaseEntities entities = new SHOPDatabaseEntities();

            // ACT
            List<SUPPLIER> list = supplierControl.GetSuppliers();
            int count = entities.SUPPLIER.Count(x => x.ACTIVE == true);

            // ASSERT
            Assert.That(list.Count, Is.EqualTo(count));
        }
    }
}
