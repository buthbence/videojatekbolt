var searchData=
[
  ['reserved',['RESERVED',['../class_d_a_l_1_1_s_a_l_e.html#a699ed8b0214ae91a6e3c1311e5eb7698',1,'DAL::SALE']]],
  ['reserveditemslist',['ReservedItemsList',['../class_bll_1_1_sales_control.html#a4a8f854a0580754d899f725b1342e155',1,'Bll::SalesControl']]],
  ['restock',['RESTOCK',['../class_d_a_l_1_1_p_r_o_d_u_c_t.html#ab79a7e84e0da52b0d93178ef9036550a',1,'DAL.PRODUCT.RESTOCK()'],['../class_d_a_l_1_1_s_h_o_p_database_entities.html#a6d33bb7e65c36b22f03680578004dc56',1,'DAL.SHOPDatabaseEntities.RESTOCK()'],['../class_d_a_l_1_1_s_u_p_p_l_i_e_r.html#a18ff02c2056e6c48ae197315ff5e247d',1,'DAL.SUPPLIER.RESTOCK()']]],
  ['restockamount',['RestockAmount',['../class_w_p_flayer_1_1_restock_confirm_window_view_model.html#a884619e9cd946b323eac6792fc1ccaec',1,'WPFlayer::RestockConfirmWindowViewModel']]],
  ['rid',['RID',['../class_d_a_l_1_1_r_e_s_t_o_c_k.html#a87ded4fd3e80f7879651bfd2ce688118',1,'DAL::RESTOCK']]]
];
