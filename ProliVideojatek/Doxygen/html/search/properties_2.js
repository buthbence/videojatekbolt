var searchData=
[
  ['cat_5fid',['CAT_ID',['../class_d_a_l_1_1_c_a_t_e_g_o_r_y.html#a531489d7c8870bdf9e2c673b0a482ec1',1,'DAL.CATEGORY.CAT_ID()'],['../class_d_a_l_1_1_s_u_b___c_a_t_e_g_o_r_y.html#ace6d0cd2f71d86b8bc31865505c1ed03',1,'DAL.SUB_CATEGORY.CAT_ID()']]],
  ['cat_5fname',['CAT_NAME',['../class_d_a_l_1_1_c_a_t_e_g_o_r_y.html#a2f52917119b5c4e5f4c3ce4f44337443',1,'DAL::CATEGORY']]],
  ['categories',['Categories',['../class_bll_1_1_product_control.html#a9517d85e1d1f887c2c6db8aa5abe4b69',1,'Bll::ProductControl']]],
  ['category',['CATEGORY',['../class_d_a_l_1_1_s_h_o_p_database_entities.html#ab95b07af102ad70c874fc8d2cc579f8c',1,'DAL.SHOPDatabaseEntities.CATEGORY()'],['../class_d_a_l_1_1_s_u_b___c_a_t_e_g_o_r_y.html#a507100e0e05982cbb02ccdec5812aab2',1,'DAL.SUB_CATEGORY.CATEGORY()']]],
  ['cid',['CID',['../class_d_a_l_1_1_c_u_s_t_o_m_e_r.html#aff88c38abbdcd76e04c3e0d9969f2df9',1,'DAL.CUSTOMER.CID()'],['../class_d_a_l_1_1_s_a_l_e.html#a9cb226e35da15a30b1c325f7f2e65854',1,'DAL.SALE.CID()']]],
  ['cname',['CNAME',['../class_d_a_l_1_1_c_u_s_t_o_m_e_r.html#a62b63b85903c8fbc68c663fa836e28d8',1,'DAL.CUSTOMER.CNAME()'],['../class_w_p_flayer_1_1_reserved_items_window_model_1_1_reservation.html#a2bd17393a0fae9846424b930e1c19347',1,'WPFlayer.ReservedItemsWindowModel.Reservation.CName()']]],
  ['customer',['CUSTOMER',['../class_d_a_l_1_1_s_a_l_e.html#aea1c0adcc7309fa4eb9bf635df56cc28',1,'DAL.SALE.CUSTOMER()'],['../class_d_a_l_1_1_s_h_o_p_database_entities.html#a552cd3a4863a4320833c7ff87479f567',1,'DAL.SHOPDatabaseEntities.CUSTOMER()']]]
];
