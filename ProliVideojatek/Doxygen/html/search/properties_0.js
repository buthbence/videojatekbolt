var searchData=
[
  ['active',['ACTIVE',['../class_d_a_l_1_1_c_u_s_t_o_m_e_r.html#a39c4d2e50f385d181d22aa98710cac71',1,'DAL.CUSTOMER.ACTIVE()'],['../class_d_a_l_1_1_e_m_p_l_o_y_e_e.html#a8f1fbe36528d4ab178249a93ffc55d2b',1,'DAL.EMPLOYEE.ACTIVE()'],['../class_d_a_l_1_1_p_r_o_d_u_c_t.html#a57aa5b72da72d4ff161827a9c8d9554e',1,'DAL.PRODUCT.ACTIVE()'],['../class_d_a_l_1_1_s_u_p_p_l_i_e_r.html#a2fd2a20584464492b21f965043d94b71',1,'DAL.SUPPLIER.ACTIVE()']]],
  ['activecustomers',['ActiveCustomers',['../class_bll_1_1_customer_control.html#aedc201f87727a8a4b0b7a943b7af680c',1,'Bll::CustomerControl']]],
  ['actualproduct',['ActualProduct',['../class_bll_1_1_product_already_in_cart_exception.html#ad61a012f73b99251a8028e22f9092b09',1,'Bll.ProductAlreadyInCartException.ActualProduct()'],['../class_bll_1_1_product_is_not_in_stock_exception.html#a057d656a640566d90a5048226d0c204c',1,'Bll.ProductIsNotInStockException.ActualProduct()']]],
  ['addr',['ADDR',['../class_d_a_l_1_1_c_u_s_t_o_m_e_r.html#ae7f6eae60afec37e6b43fe4809c234c0',1,'DAL::CUSTOMER']]]
];
