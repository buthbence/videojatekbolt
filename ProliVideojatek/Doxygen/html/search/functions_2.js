var searchData=
[
  ['decreasestock',['DecreaseStock',['../interface_d_a_l_1_1_intefaces_1_1_i_p_r_o_d_u_c_t_repository.html#ae0eb2f37bb97b9e31b5b3cbe7dfbadb6',1,'DAL.Intefaces.IPRODUCTRepository.DecreaseStock()'],['../class_d_a_l_1_1_repos_1_1_product_repository.html#a4fed840003bf95aafc465d2c627e2c5d',1,'DAL.Repos.ProductRepository.DecreaseStock()']]],
  ['deleteemployee',['DeleteEmployee',['../class_bll_1_1_employee_control.html#a9594b3c80d3eca1aebc93d1c9b91f826',1,'Bll::EmployeeControl']]],
  ['deleteemployee_5fshould_5fsetactivetofalse_5fwhen_5fremove',['DeleteEmployee_Should_SetActiveToFalse_When_Remove',['../class_proli_video_jatek_tests_1_1_employee_control_tests.html#a5b2c9432905420482b9c10fa0ede3af3',1,'ProliVideoJatekTests::EmployeeControlTests']]],
  ['deleteproductfromreservation',['DeleteProductFromReservation',['../class_bll_1_1_sales_control.html#ad4d6c6284a961aa9287bd16e6e0545c3',1,'Bll::SalesControl']]],
  ['deletereservation',['DeleteReservation',['../class_bll_1_1_sales_control.html#ab69f2421469c4626dd0a216b3754b8d1',1,'Bll::SalesControl']]],
  ['deleterestockrequest',['DeleteRestockRequest',['../class_bll_1_1_product_control.html#a0ddc75fff42ed5875b2cf962c7b70d6b',1,'Bll::ProductControl']]],
  ['deletesupplier',['DeleteSupplier',['../class_bll_1_1_supplier_control.html#ab4cecf56e84ab62c5734e59c7fc06ac0',1,'Bll::SupplierControl']]]
];
