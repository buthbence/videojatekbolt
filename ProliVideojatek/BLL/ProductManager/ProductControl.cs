﻿// <copyright file="ProductControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DAL;
    using DAL.Intefaces;
    using DAL.Repos;

    /// <summary>
    /// BLL class for product management.
    /// </summary>
    public class ProductControl
    {
        private IPRODUCTRepository products;
        private ISUPPLIERRepository suppliers;
        private ISUPPRODRepository supProd;
        private IRESTOCKRepository restocks;
        private ICATEGORYRepository categories;
        private ISUB_CATEGORYRepository subCategories;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductControl"/> class.
        /// </summary>
        /// <param name="products">Product repository interface</param>
        /// <param name="suppliers">Supplier repository interface</param>
        /// <param name="supProd">SupProd repository interface</param>
        /// <param name="restocks">Restock repository interface</param>
        /// <param name="categories">Category repository interface</param>
        /// <param name="subcategories">Subcategory repository interface</param>
        public ProductControl(IPRODUCTRepository products, ISUPPLIERRepository suppliers, ISUPPRODRepository supProd, IRESTOCKRepository restocks, ICATEGORYRepository categories, ISUB_CATEGORYRepository subcategories)
        {
            this.products = products;
            this.suppliers = suppliers;
            this.supProd = supProd;
            this.restocks = restocks;
            this.categories = categories;
            this.subCategories = subcategories;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductControl"/> class.
        /// </summary>
        public ProductControl()
        {
            this.products = UniversalRepository.ProductRepository;
            this.suppliers = UniversalRepository.SupplierRepository;
            this.supProd = UniversalRepository.SupProdRepository;
            this.restocks = UniversalRepository.RestockRepository;
            this.categories = UniversalRepository.CategoryRepository;
            this.subCategories = UniversalRepository.SubCategoryRepository;
        }

        /// <summary>
        /// Gets minimum product price from database.
        /// </summary>
        /// <returns>Minimun price</returns>
        public int MinimumPrice
        {
            get
            {
                var minPrice = this.products.QueryAll().Min(x => x.PRICE);
                return minPrice != null ? (int)minPrice : 0;
            }
        }

        /// <summary>
        /// Gets a free product id from database.
        /// </summary>
        /// <returns>Product id for new product</returns>
        public decimal NewProductId
        {
            get
            {
                List<PRODUCT> productList = this.products.QueryAll().ToList();
                return productList.Count != 0 ? productList.Max(x => x.PID) + 1 : 1;
            }
        }

        /// <summary>
        /// Gets categories from database.
        /// </summary>
        /// <returns>List of categories</returns>
        public List<CATEGORY> Categories
        {
            get
            {
                List<CATEGORY> categoryList = new List<CATEGORY>();
                categoryList.Add(new CATEGORY() { CAT_NAME = "Any" });
                categoryList.AddRange(this.categories.QueryAll());
                return categoryList;
            }
        }

        /// <summary>
        /// Gets maximum product price from database.
        /// </summary>
        /// <returns>Maximum price</returns>
        public int MaximumPrice
        {
            get
            {
                var maxPrice = this.products.QueryAll().Max(x => x.PRICE);
                return maxPrice != null ? (int)maxPrice : 0;
            }
        }

        /// <summary>
        /// Returns products from database filtered by search term, minimum price, maximum price, categroy and subcategory.
        /// </summary>
        /// <param name="searchTerm">Search term used for filtering</param>
        /// <param name="minPrice">Minimum price used for filtering</param>
        /// <param name="maxPrice">Maximum price used for filtering</param>
        /// <param name="onlyAvailable">Boolean used for returning only products that are available in stock</param>
        /// <param name="category">Category used for filtering</param>
        /// <param name="subCategory">Sub Category used for filtering</param>
        /// <returns>List of filtered products</returns>
        public List<PRODUCT> GetProducts(string searchTerm, int minPrice, int maxPrice, bool onlyAvailable, CATEGORY category, SUB_CATEGORY subCategory)
        {
            var filteredProducts = from akt in this.products.QueryAll()
                                   where akt.ACTIVE && (searchTerm == null ||
                                   (akt.PID.ToString().ToLower().Contains(searchTerm) ||
                                   akt.PNAME.ToLower().Contains(searchTerm) ||
                                   akt.SUB_CATEGORY.CATEGORY.CAT_NAME.ToLower().Contains(searchTerm) ||
                                   akt.SUB_CATEGORY.SUB_CAT_NAME.ToLower().Contains(searchTerm))) &&
                                   minPrice <= akt.PRICE && akt.PRICE <= maxPrice
                                   orderby akt.PID ascending
                                   select akt;
            if (category != null)
            {
                var filteredByCategory = from akt in filteredProducts
                                         where category.CAT_ID == 0 || category.CAT_ID == akt.SUB_CATEGORY.CAT_ID
                                         select akt;
                if (subCategory != null)
                {
                    var filteredBySubCategory = from akt in filteredByCategory
                                                where subCategory.SUB_CAT_ID == 0 || subCategory.SUB_CAT_ID == akt.SUB_CATEGORY.SUB_CAT_ID
                                                select akt;
                    return onlyAvailable ? filteredBySubCategory.Where(x => x.STOCK > 0).ToList() : filteredBySubCategory.ToList();
                }
                else
                {
                    return onlyAvailable ? filteredByCategory.Where(x => x.STOCK > 0).ToList() : filteredByCategory.ToList();
                }
            }

            return onlyAvailable ? filteredProducts.Where(x => x.STOCK > 0).ToList() : filteredProducts.ToList();
        }

        /// <summary>
        /// Gets subcategories of a given category from database.
        /// </summary>
        /// <param name="selectedCategory">Selected category used for filtering</param>
        /// <returns>List of subcategories</returns>
        public List<SUB_CATEGORY> GetSubcategories(CATEGORY selectedCategory)
        {
            List<SUB_CATEGORY> subCategoriesList = new List<SUB_CATEGORY>();
            subCategoriesList.Add(new SUB_CATEGORY() { SUB_CAT_NAME = "Any" });
            subCategoriesList.AddRange(this.subCategories.QueryAll().Where(x => selectedCategory.CAT_ID == 0 || x.CATEGORY.CAT_ID == selectedCategory.CAT_ID));
            return subCategoriesList;
        }

        /// <summary>
        /// Changes a products data in database.
        /// </summary>
        /// <param name="productToChange">Product to change</param>
        public void ChangeProductData(PRODUCT productToChange)
        {
            PRODUCT originalProduct = this.products.QueryAll().Single(x => x.PID == productToChange.PID);

            if (productToChange.PNAME != originalProduct.PNAME)
            {
                this.products.ChangeName(productToChange.PID, productToChange.PNAME);
            }

            if (productToChange.PSUBCAT != originalProduct.PSUBCAT)
            {
                this.products.ChangeSubCategory(productToChange.PID, productToChange.PSUBCAT);
            }

            if (productToChange.PRICE != originalProduct.PRICE)
            {
                this.products.ChangePrice(productToChange.PID, (int)productToChange.PRICE);
            }

            if (productToChange.DISCOUNT != originalProduct.DISCOUNT)
            {
                this.products.ChangeDiscount(productToChange.PID, (int)productToChange.DISCOUNT);
            }
        }

        /// <summary>
        /// Gets suppliers of a product from database.
        /// </summary>
        /// <param name="productId">Products id</param>
        /// <returns>List of suppliers</returns>
        public List<SUPPROD> GetSuppliers(decimal productId)
        {
            List<SUPPLIER> sup = (from item in this.supProd.QueryAll()
                                  where item.PID == productId
                                  select item.SUPPLIER).ToList();

            List<SUPPLIER> allSuppliers = this.suppliers.QueryAll().Where(x => x.ACTIVE == true).ToList();

            List<SUPPROD> result = new List<SUPPROD>();

            foreach (var item in allSuppliers)
            {
                if (sup.Count(x => x.SID == item.SID) > 0)
                {
                    result.Add(new SUPPROD() { SID = item.SID, SUPPLIER = item, PID = productId });
                }
                else
                {
                    result.Add(new SUPPROD() { SID = item.SID, SUPPLIER = item });
                }
            }

            return result;
        }

        /// <summary>
        /// Gets all suppliers from database.
        /// </summary>
        /// <returns>List of suppliers</returns>
        public List<SUPPROD> GetSuppliers()
        {
            List<SUPPLIER> allSuppliers = this.suppliers.QueryAll().Where(x => x.ACTIVE == true).ToList();

            List<SUPPROD> result = new List<SUPPROD>();

            foreach (var item in allSuppliers)
            {
                result.Add(new SUPPROD() { SID = item.SID, SUPPLIER = item });
            }

            return result;
        }

        /// <summary>
        /// Updates supplier product relation in database.
        /// </summary>
        /// <param name="productId">Products id</param>
        /// <param name="productSuppliers">Suppliers of the selected product</param>
        public void UpdateSupprodData(decimal productId, List<SUPPROD> productSuppliers)
        {
            var supprodsInDatabase = this.supProd.QueryAll().Where(x => x.PID == productId).ToList();

            List<SUPPROD> supProdsToAdd = new List<SUPPROD>();
            List<SUPPROD> supProdsToRemove = new List<SUPPROD>();

            foreach (SUPPROD item in productSuppliers)
            {
                if (item.PID != 0 && supprodsInDatabase.Count(x => x.SID == item.SID) == 0)
                {
                    supProdsToAdd.Add(new SUPPROD() { PID = productId, SID = item.SID });
                }
                else if (item.PID == 0 && supprodsInDatabase.Count(x => x.SID == item.SID) > 0)
                {
                    supProdsToRemove.Add(item);
                }
            }

            foreach (SUPPROD item in supProdsToRemove)
            {
                this.supProd.Remove(this.supProd.QueryAll().Single(x => x.PID == productId && x.SID == item.SID).SPID);
            }

            List<SUPPROD> supProdList = this.supProd.QueryAll().ToList();
            decimal id = supProdList.Count != 0 ? this.supProd.QueryAll().Max(x => x.SPID) + 1 : 1;

            foreach (SUPPROD item in supProdsToAdd)
            {
                this.supProd.Insert(new SUPPROD() { SPID = id, PID = item.PID, SID = item.SID });
                id++;
            }
        }

        /// <summary>
        /// Adds new product to database.
        /// </summary>
        /// <param name="newProduct">New product to add</param>
        public void AddNewProduct(PRODUCT newProduct)
        {
            this.products.Insert(newProduct);
        }

        /// <summary>
        /// Removes a product from database.
        /// </summary>
        /// <param name="productToRemove">Product to remove</param>
        public void RemoveProduct(PRODUCT productToRemove)
        {
            var supprodIds = (from item in this.supProd.QueryAll()
                              where item.PID == productToRemove.PID
                              select item.SPID).ToList();
            foreach (var item in supprodIds)
            {
                this.supProd.Remove(item);
            }

            this.products.Remove(productToRemove.PID);
        }

        /// <summary>
        /// Inserts a new Restock record to database
        /// </summary>
        /// <param name="pid">Product id</param>
        /// <param name="sid">Supplier id</param>
        /// <param name="amount">Quantity to restock</param>
        public void InsertRestock(decimal pid, decimal sid, decimal amount)
        {
            decimal id = this.restocks.QueryAll().Count() > 0 ? this.restocks.QueryAll().ToList().Max(x => x.RID) + 1 : 1;
            this.restocks.Insert(new RESTOCK() { RID = id, PID = pid, SID = sid, QUANTITY = amount, DATE = DateTime.Now });
        }

        /// <summary>
        /// Gets a list of restock requests and filters it using the search term.
        /// </summary>
        /// <param name="searchTerm">The search term used for filtering.</param>
        /// <returns>The filtered list.</returns>
        public List<RESTOCK> GetRestockList(string searchTerm)
        {
            var restockResults = from akt in this.restocks.QueryAll()
                                 where searchTerm == null ||
                                 akt.PRODUCT.PNAME.Contains(searchTerm) ||
                                 akt.RID.ToString().Contains(searchTerm) ||
                                 akt.SUPPLIER.SNAME.Contains(searchTerm)
                                 select akt;
            return restockResults.ToList();
        }

        /// <summary>
        /// Saves the changes made on the window.
        /// </summary>
        /// <param name="changes">The items we have marked as received.</param>
        public void SaveRestockChanges(List<RESTOCK> changes)
        {
            foreach (var akt in changes)
            {
                this.products.IncreaseStock((decimal)akt.PID, (int)akt.QUANTITY);
                this.restocks.Remove(akt.RID);
            }
        }

        /// <summary>
        /// Deletes the selected restock request.
        /// </summary>
        /// <param name="selected">The selected restock request.</param>
        public void DeleteRestockRequest(RESTOCK selected)
        {
            this.restocks.Remove(selected.RID);
        }
    }
}
