﻿// <copyright file="SupplierControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL;
    using DAL.Intefaces;
    using DAL.Repos;

    /// <summary>
    /// Business logic for suplier managenemt.
    /// </summary>
    public class SupplierControl
    {
        private ISUPPLIERRepository suppliers;

        /// <summary>
        /// Initializes a new instance of the <see cref="SupplierControl"/> class.
        /// </summary>
        public SupplierControl()
        {
            this.suppliers = UniversalRepository.SupplierRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SupplierControl"/> class.
        /// </summary>
        /// <param name="supplierRepo">Supplier repository</param>
        public SupplierControl(ISUPPLIERRepository supplierRepo)
        {
            this.suppliers = supplierRepo;
        }

        /// <summary>
        /// Gets all suppliers from the database.
        /// </summary>
        /// <returns>List of SUPPLIER objects</returns>
        public List<SUPPLIER> GetSuppliers()
        {
            var allSuppliers = from supplier in this.suppliers.QueryAll()
                            where supplier.ACTIVE == true
                           select supplier;
            return allSuppliers.ToList();
        }

        /// <summary>
        /// Adds a new supplier to the database.
        /// </summary>
        /// <param name="newSupplier">The supplier to add</param>
        public void AddNewSupplier(SUPPLIER newSupplier)
        {
            newSupplier.ACTIVE = true;
            newSupplier.SID = this.GetNewId();
            this.suppliers.Insert(newSupplier);
        }

        /// <summary>
        /// Updates the specified suppliers info
        /// </summary>
        /// <param name="selectedSupplier">The supplier to edit, with the nwe informations.</param>
        public void EditSupplier(SUPPLIER selectedSupplier)
        {
            this.suppliers.ChangeName(selectedSupplier.SID, selectedSupplier.SNAME);
            this.suppliers.ChangeEmail(selectedSupplier.SID, selectedSupplier.SEMAIL);
            this.suppliers.ChangePhone(selectedSupplier.SID, selectedSupplier.SPHONE);
        }

        /// <summary>
        /// Returns a list of suppliers, filtered with a search term.
        /// </summary>
        /// <param name="supplierSearchTerm">The searching keyword</param>
        /// <returns>List of filtered suppliers.</returns>
        public List<SUPPLIER> GetSuppliers(string supplierSearchTerm)
        {
            List<SUPPLIER> sups = this.GetSuppliers();
            List<SUPPLIER> result = new List<SUPPLIER>();
            supplierSearchTerm = supplierSearchTerm == null ? string.Empty : supplierSearchTerm;
            foreach (SUPPLIER item in sups)
            {
                if (item.SNAME.Contains(supplierSearchTerm) ||
                    item.SEMAIL.Contains(supplierSearchTerm) ||
                    item.SPHONE.Contains(supplierSearchTerm) ||
                    item.SID.ToString().Contains(supplierSearchTerm))
                {
                    result.Add(item);
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes a supplier.
        /// </summary>
        /// <param name="selectedSupplier">The supplier to delete.</param>
        public void DeleteSupplier(SUPPLIER selectedSupplier)
        {
            this.suppliers.Remove(selectedSupplier.SID);
        }

        private decimal GetNewId()
        {
            var idS = from supplier in this.suppliers.QueryAll()
                      select supplier.SID;
            return idS.FirstOrDefault() == 0 ? 1 : idS.Max() + 1;
        }
    }
}
