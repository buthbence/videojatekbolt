﻿// <copyright file="ProductIsNotInStockException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Runtime.Serialization;
    using DAL;

    /// <summary>
    /// Exception to throw, when somebody tries to add an item to cart, but there is no enough item in stock.
    /// </summary>
    [Serializable]
    public class ProductIsNotInStockException : Exception
    {
        private PRODUCT actualProduct;
        private int wrongQuantity;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductIsNotInStockException"/> class.
        /// </summary>
        /// <param name="actualProduct">The product which caused the problem.</param>
        /// <param name="wrongQuantity">Wrong quantity, because there's no enough item in stock.</param>
        public ProductIsNotInStockException(PRODUCT actualProduct, int wrongQuantity)
        {
            this.actualProduct = actualProduct;
            this.wrongQuantity = wrongQuantity;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductIsNotInStockException"/> class.
        /// </summary>
        /// <param name="actualProduct">The product which caused the problem.</param>
        /// <param name="wrongQuantity">Wrong quantity, because there's no enough item in stock.</param>
        /// <param name="message">Message for debug.</param>
        public ProductIsNotInStockException(PRODUCT actualProduct, int wrongQuantity, string message)
            : base(message)
        {
            this.actualProduct = actualProduct;
            this.wrongQuantity = wrongQuantity;
        }

        /// <summary>
        /// Gets the product which caused the problem.
        /// </summary>
        public PRODUCT ActualProduct
        {
            get
            {
                return this.actualProduct;
            }
        }

        /// <summary>
        /// Gets the wrong quantity.
        /// </summary>
        public int WrongQuantity
        {
            get
            {
                return this.wrongQuantity;
            }
        }

        /// <inheritdoc/>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
