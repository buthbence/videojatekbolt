﻿// <copyright file="ProductAlreadyInCartException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Runtime.Serialization;
    using DAL;

    /// <summary>
    /// Exception when trying to add an item to a cart, which already contains that item.
    /// </summary>
    [Serializable]
    public class ProductAlreadyInCartException : Exception
    {
        private PRODUCT actualProduct;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductAlreadyInCartException"/> class.
        /// </summary>
        /// <param name="actualProduct">The product which caused the problem</param>
        public ProductAlreadyInCartException(PRODUCT actualProduct)
        {
            this.actualProduct = actualProduct;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductAlreadyInCartException"/> class.
        ///
        /// </summary>
        /// <param name="actualProduct">The product which caused the problem</param>
        /// <param name="message">Message for debug</param>
        public ProductAlreadyInCartException(PRODUCT actualProduct, string message)
            : base(message)
        {
            this.actualProduct = actualProduct;
        }

        /// <summary>
        /// Gets the product which caused the problem
        /// </summary>
        public PRODUCT ActualProduct
        {
            get { return this.actualProduct; }
        }

        /// <inheritdoc/>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
