﻿// <copyright file="SalesControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DAL;
    using DAL.Intefaces;
    using DAL.Repos;

    /// <summary>
    /// Business logic for selling and reserving products.
    /// </summary>
    public class SalesControl
    {
        private ISALERepository sales;
        private ISOLDITEMRepository solditems;
        private ICUSTOMERRepository customers;
        private IPRODUCTRepository products;

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesControl"/> class.
        /// </summary>
        public SalesControl()
        {
            this.sales = UniversalRepository.SaleRepository;
            this.solditems = UniversalRepository.SoldItemRepository;
            this.customers = UniversalRepository.CustomerRepository;
            this.products = UniversalRepository.ProductRepository;
        }

        /// <summary>
        /// Gets all reservations info to show
        /// </summary>
        /// <returns>Enumerable collection of objects with 3 properties (GID: reservation ID, TotalPrice: price of reservation, CNAME: customer name)</returns>
        public IEnumerable<object> ReservedItemsList
        {
            get
            {
                var reservations = (from sale in this.sales.QueryAll().ToList()
                                    where sale.RESERVED == true
                                    join customer in this.customers.QueryAll().ToList() on sale.CID equals customer.CID
                                    select new
                                    {
                                        GID = sale.SALID,
                                        TotalPrice = this.CartTotalPrice(
                                            sale.SOLDITEM.ToDictionary<SOLDITEM, PRODUCT, int>(
                                                x => x.PRODUCT,
                                                y => Convert.ToInt32(y.QUANTITY)),
                                            customer),
                                        CName = customer.CNAME
                                    }).ToList();
                return reservations;
            }
        }

        /// <summary>
        /// Returns the reserved products for one reservation
        /// </summary>
        /// <param name="reservationId">Reservation ID</param>
        /// <returns>A Dictionary, keys are PRODUCT objects, values show the reserved quantity of the actual product.</returns>
        public Dictionary<PRODUCT, int> GetReservedProducts(decimal reservationId)
        {
            Dictionary<PRODUCT, int> reservedProducts = new Dictionary<PRODUCT, int>();
            var reservations = from x in this.solditems.QueryAll()
                               where x.SALID == reservationId
                               select new { prod = x.PRODUCT, qty = x.QUANTITY };
            foreach (var item in reservations)
            {
                reservedProducts.Add(item.prod, Convert.ToInt32(item.qty));
            }

            return reservedProducts;
        }

        /// <summary>
        /// Caltulates the total price for a cart (could be reservation or sale), with loyalty discounts.
        /// </summary>
        /// <param name="cart">Dictionry with the ordered products and quantities</param>
        /// <param name="selectedCustomer">The customer who buys the cart.</param>
        /// <returns>The total price to pay.</returns>
        public int CartTotalPrice(Dictionary<PRODUCT, int> cart, CUSTOMER selectedCustomer)
        {
            int totalPrice = 0;
            foreach (KeyValuePair<PRODUCT, int> item in cart)
            {
                totalPrice += this.CalculatePrice(item, selectedCustomer);
            }

            return totalPrice;
        }

        /// <summary>
        /// Finalizes a reservation
        /// </summary>
        /// <param name="gId">Reservation ID to sell</param>
        public void SellReservation(decimal gId)
        {
            this.sales.ChangeReservationStatus(gId);
        }

        /// <summary>
        /// Calculates the total prices for a cart (reservation or sale), without discounts
        /// </summary>
        /// <param name="cart">The cart which price needs to be calculated</param>
        /// <returns>The total price of the elements in the cart</returns>
        public int CartTotalPrice(Dictionary<PRODUCT, int> cart)
        {
            return this.CartTotalPrice(cart, null);
        }

        /// <summary>
        /// Calculates the total prices for a cart (reservation or sale)
        /// </summary>
        /// <param name="cart">The cart which price needs to be calculated</param>
        /// <param name="forLoyalCustomer">Has the customer have loyalty card.</param>
        /// <returns>The total price to pay.</returns>
        public int CartTotalPrice(Dictionary<PRODUCT, int> cart, bool forLoyalCustomer)
        {
            return forLoyalCustomer ? this.CartTotalPrice(cart, new CUSTOMER() { LCARD = true }) : this.CartTotalPrice(cart);
        }

        /// <summary>
        /// Sells or reserves items for a specified customer by an employee.
        /// </summary>
        /// <param name="cart">Products to sell (dictionary keys) with quantities (dictionary values)</param>
        /// <param name="selectedCustomer">The customer to sell for</param>
        /// <param name="employee">The employee who sells the products</param>
        /// <param name="isReservation">True, if not selling just reserving</param>
        /// <returns>The id of the new SALE item.</returns>
        public decimal Sell(Dictionary<PRODUCT, int> cart, CUSTOMER selectedCustomer, EMPLOYEE employee, bool isReservation)
        {
            decimal newSaleID = this.GetNewSaleId();
            this.sales.Insert(new SALE()
            {
                SALID = newSaleID,
                CID = selectedCustomer.CID,
                DATE = DateTime.Now,
                RESERVED = isReservation,
                EID = employee.EID
            });
            foreach (KeyValuePair<PRODUCT, int> item in cart)
            {
                this.solditems.Insert(new SOLDITEM()
                {
                     SOLDID = this.GetNewSolditemId(),
                     SALID = newSaleID,
                     PID = item.Key.PID,
                     QUANTITY = item.Value,
                     PRICE = this.CalculatePrice(item, selectedCustomer)
                });
            }

            this.ReduceStock(cart);
            return newSaleID;
        }

        /// <summary>
        /// Deletes a reservation.
        /// </summary>
        /// <param name="gId">Reservation ID to delete</param>
        public void DeleteReservation(decimal gId)
        {
            var cart = from x in this.solditems.QueryAll()
                       where x.SALID == gId
                       select new { soldId = x.SOLDID, pId = x.PRODUCT.PID, qty = x.QUANTITY };
            foreach (var item in cart.ToList())
            {
                this.solditems.Remove(item.soldId);
                this.products.IncreaseStock(item.pId, Convert.ToInt32(item.qty));
            }

            this.sales.Remove(gId);
        }

        /// <summary>
        /// Deletes a product from a reservation, and decides whether the reservation is empty (has no products)
        /// </summary>
        /// <param name="rId">Id of the reservation (sale)</param>
        /// <param name="pId">Id of product</param>
        /// <returns>True, if the reservation has no other products, otherwise returns false</returns>
        public bool DeleteProductFromReservation(decimal rId, decimal pId)
        {
            var id = from x in this.solditems.QueryAll()
                     where x.SALID == rId && x.PID == pId
                     select new { soldId = x.SOLDID, pid = x.PRODUCT.PID, amount = x.QUANTITY };
            var deleteInfo = id.FirstOrDefault();
            this.solditems.Remove(deleteInfo.soldId);
            this.products.IncreaseStock(deleteInfo.pid, Convert.ToInt32(deleteInfo.amount));
            var saleIds = from y in this.solditems.QueryAll()
                          select y.SALID;
            return !saleIds.Contains(rId);
        }

        /// <summary>
        /// Adds a product to a cart.
        /// </summary>
        /// <param name="cart">The cart (keys are products, values are the quantities)</param>
        /// <param name="productId">ID of the product to add</param>
        /// <param name="quantity">How many items to add</param>
        /// <returns>The cart with the new product in it.</returns>
        public Dictionary<PRODUCT, int> AddToCart(Dictionary<PRODUCT, int> cart, decimal productId, int quantity)
        {
            Dictionary<PRODUCT, int> newCart = new Dictionary<PRODUCT, int>(cart);
            PRODUCT productToAdd = (from x in this.products.QueryAll()
                                    where x.PID == productId
                                    select x).FirstOrDefault();
            if (newCart.ContainsKey(productToAdd))
            {
                throw new ProductAlreadyInCartException(productToAdd);
            }

            if (this.IsInStock(productId, quantity))
            {
                newCart.Add(productToAdd, quantity);
            }
            else
            {
                throw new ProductIsNotInStockException(productToAdd, quantity);
            }

            return newCart;
        }

        /// <summary>
        /// Edits a products quantity in a cart
        /// </summary>
        /// <param name="cart">The cart wich contains the product(keys are products, values are the quantities)</param>
        /// <param name="product">The product wich quantity needs to be edited</param>
        /// <param name="newQuantity">New quantity for the product</param>
        /// <returns>The cart with the edited quantity</returns>
        public Dictionary<PRODUCT, int> EditQuantity(Dictionary<PRODUCT, int> cart, PRODUCT product, int newQuantity)
        {
            if (this.IsInStock(product.PID, newQuantity))
            {
                Dictionary<PRODUCT, int> editedCart = new Dictionary<PRODUCT, int>(cart);
                editedCart[product] = newQuantity;
                return editedCart;
            }

            throw new ProductIsNotInStockException(product, newQuantity);
        }

        /// <summary>
        /// Searches for products, which are often bought with the given product
        /// </summary>
        /// <param name="selectedProduct">The given product</param>
        /// <returns>List of products, which are often bought with the given product</returns>
        public List<PRODUCT> GetOftenBoughtTogetherProducts(PRODUCT selectedProduct)
        {
            Dictionary<PRODUCT, int> occurrences = new Dictionary<PRODUCT, int>();
            var allProdIds = from x in this.products.QueryAll() select x;
            foreach (PRODUCT item in allProdIds.ToList())
            {
                occurrences[item] = 0;
            }

            var sales = from sale in this.sales.QueryAll()
                        select sale.SALID;
            foreach (decimal actualSaleId in sales.ToList())
            {
                var productsInActualSale = (from solditem in this.solditems.QueryAll()
                                           where solditem.SALID == actualSaleId
                                           select solditem.PRODUCT).ToList();
                if (productsInActualSale.Contains(selectedProduct))
                {
                    foreach (PRODUCT prod in productsInActualSale.ToList())
                    {
                        occurrences[prod]++;
                    }
                }
            }

            occurrences.Remove(selectedProduct);
            List<PRODUCT> oftenBoughtWithSelected = new List<PRODUCT>();
            bool noMoreItems = false;
            for (int i = 0; i < 5 && occurrences.Count > 0 && !noMoreItems; i++)
            {
                PRODUCT p = occurrences.FirstOrDefault(y => y.Value == occurrences.Values.Max()).Key;
                var maxOccurrenceProd = (from x in this.products.QueryAll()
                                         where x.PID == p.PID
                                         select x).FirstOrDefault();
                noMoreItems = occurrences[maxOccurrenceProd] == 0 ? true : false;
                if (!noMoreItems)
                {
                    oftenBoughtWithSelected.Add(maxOccurrenceProd);
                    occurrences.Remove(maxOccurrenceProd);
                }
            }

            return oftenBoughtWithSelected;
        }

        /// <summary>
        /// Reservations can expire. This method moves them back into stock.
        /// </summary>
        public void FreeUpExpiredReserves()
        {
            var reservedItems = this.sales.QueryAll().Where(x => x.RESERVED).ToList();
            for (int i = reservedItems.Count() - 1; i >= 0; i--)
            {
                if (reservedItems[i].DATE < DateTime.Now.AddDays(-5))
                {
                    this.DeleteReservation(reservedItems[i].SALID);
                }
            }
        }

        private bool IsInStock(decimal productId, int quantity)
        {
            PRODUCT product = (from x in this.products.QueryAll()
                               where x.PID.Equals(productId)
                               select x).FirstOrDefault();
            return product.STOCK >= quantity;
        }

        private decimal GetNewSaleId()
        {
            var ids = from x in this.sales.QueryAll()
                      select x.SALID;
            decimal maxId = ids.Count() == 0 ? 0 : ids.Max();
            return maxId + 1;
        }

        private void ReduceStock(Dictionary<PRODUCT, int> cart)
        {
            foreach (KeyValuePair<PRODUCT, int> item in cart)
            {
                this.products.DecreaseStock(item.Key.PID, item.Value);
            }
        }

        private int CalculatePrice(KeyValuePair<PRODUCT, int> item, CUSTOMER selectedCustomer)
        {
            int price = 0;
            if (item.Key.PRICE.HasValue && item.Key.PRICE.Value > 0)
            {
                price = Convert.ToInt32(item.Key.PRICE) * item.Value;
                if (item.Key.DISCOUNT.HasValue &&
                    item.Key.DISCOUNT > 0 &&
                    selectedCustomer != null &&
                    selectedCustomer.LCARD.HasValue ? selectedCustomer.LCARD.Value : false)
                {
                    double discountMultiplier = (100 - Convert.ToDouble(item.Key.DISCOUNT)) / 100;
                    price = (int)(price * discountMultiplier);
                }
            }

            return price;
        }

        private decimal GetNewSolditemId()
        {
            var ids = from x in this.solditems.QueryAll()
                      select x.SOLDID;
            decimal maxId = ids.Count() == 0 ? 0 : ids.Max();
            return maxId + 1;
        }
    }
}
