﻿// <copyright file="PrintControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using DAL;
    using DAL.Intefaces;
    using DAL.Repos;

    /// <summary>
    /// Bll class from print management.
    /// </summary>
    public class PrintControl
    {
        private ISALERepository saleRepo;
        private ISOLDITEMRepository solditemRepo;
        private ICUSTOMERRepository customerRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrintControl"/> class.
        /// </summary>
        public PrintControl()
        {
            this.saleRepo = UniversalRepository.SaleRepository;
            this.solditemRepo = UniversalRepository.SoldItemRepository;
            this.customerRepo = UniversalRepository.CustomerRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PrintControl"/> class.
        /// </summary>
        /// <param name="saleRepo">SaleRepository interface.</param>
        /// <param name="solditemRepo">SoldItemRepository interface.</param>
        /// <param name="customerRepo">CustomerRepository interface.</param>
        public PrintControl(ISALERepository saleRepo, ISOLDITEMRepository solditemRepo, ICUSTOMERRepository customerRepo)
        {
            this.saleRepo = saleRepo;
            this.solditemRepo = solditemRepo;
            this.customerRepo = customerRepo;
        }

        /// <summary>
        /// Generates customer invoice to print.
        /// </summary>
        /// <param name="id">Sale id</param>
        /// <returns>Customer invoice visual.</returns>
        public DrawingVisual GenerateInvoice(decimal id)
        {
            Pen pen = new Pen(Brushes.Blue, 2);

            DrawingVisual dv = new DrawingVisual();
            var dc = dv.RenderOpen();
            dc.DrawText(new FormattedText("Proli Videojatek", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily("Calibri"), FontStyles.Normal, FontWeights.Bold, FontStretches.Normal), 18, Brushes.Black), new Point(10, 10));
            dc.DrawText(new FormattedText("Budapest", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily("Calibri"), FontStyles.Normal, FontWeights.Bold, FontStretches.Normal), 13, Brushes.Black), new Point(10, 35));
            dc.DrawText(new FormattedText("+36304556677", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily("Calibri"), FontStyles.Normal, FontWeights.Bold, FontStretches.Normal), 13, Brushes.Black), new Point(10, 45));
            dc.DrawLine(pen, new Point(0, 65), new Point(832, 65));
            dc.DrawText(new FormattedText("Products:", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily("Calibri"), FontStyles.Oblique, FontWeights.Bold, FontStretches.Normal), 13, Brushes.Black), new Point(10, 70));

            int y = 100;
            decimal totalPrice = 0;
            StringBuilder sB = new StringBuilder();
            sB.AppendFormat(string.Format("{0,-10}{1,-50}{2,20}{3,20}\n", "ID", "Name", "Price", "Quantity"));
            foreach (var item in this.GetProducts(id))
            {
                totalPrice += (decimal)item.PRICE * (decimal)item.STOCK;
                sB.AppendFormat(string.Format("{0,-10}{1,-50}{2,20}{3,15}\n", item.PID, item.PNAME, item.PRICE, item.STOCK));
            }

            sB.AppendLine();
            sB.AppendLine();
            sB.AppendLine();
            sB.AppendFormat(string.Format("{0,-10}{1,-10}{2,20}{3,10}", "TotalPrice: ", totalPrice, "Date: ", this.GetSaleDate(id).ToShortDateString()));
            sB.AppendLine();
            sB.AppendFormat(string.Format("{0,-10}", "Customer : \n"));

            var customer = this.GetCustomer(id);

            sB.AppendFormat(string.Format("ID: {0,-10}\n", customer.CID));
            sB.AppendFormat(string.Format("{0,-10}\n", customer.CNAME));
            sB.AppendFormat(string.Format("{0,-10}\n", customer.EMAIL));
            sB.AppendFormat(string.Format("{0,-10}\n", customer.PHONE));
            sB.AppendFormat(string.Format("{0,-10}\n", customer.ADDR));
            sB.AppendFormat(string.Format("{0,-10}\n", customer.CID));
            sB.AppendLine();
            sB.AppendLine();
            sB.AppendFormat(string.Format("Sale ID: {0,-10}\n", id));
            dc.DrawText(new FormattedText(sB.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily("Courier New"), FontStyles.Normal, FontWeights.Bold, FontStretches.ExtraCondensed), 13, Brushes.Black), new Point(10, y));

            dc.Close();
            return dv;
        }

        /// <summary>
        /// Get products of a sale.
        /// </summary>
        /// <param name="id">Sale id</param>
        /// <returns>List of products</returns>
        private List<PRODUCT> GetProducts(decimal id)
        {
            var solditems = this.solditemRepo.QueryAll().Where(x => x.SALID == id);

            var products = from item in this.solditemRepo.QueryAll().Where(x => x.SALID == id)
                       select item.PRODUCT;

            foreach (var item in products)
            {
                item.PRICE = products.Where(x => x.PID == item.PID).Select(x => x.PRICE).Single();
                item.STOCK = solditems.Where(x => x.PID == item.PID).Select(x => x.QUANTITY).Single();
            }

            return products.ToList();
        }

        /// <summary>
        /// Gets the date of a sale.
        /// </summary>
        /// <param name="id">Sale id</param>
        /// <returns>Date of sale</returns>
        private DateTime GetSaleDate(decimal id)
        {
            var date = this.saleRepo.QueryAll().Where(x => x.SALID == id).Select(x => x.DATE).Single();

            return (DateTime)date;
        }

        /// <summary>
        /// Gets a sales customer.
        /// </summary>
        /// <param name="id">Sale id</param>
        /// <returns>Customer</returns>
        private CUSTOMER GetCustomer(decimal id)
        {
            var cid = this.saleRepo.QueryAll().Where(x => x.SALID == id).Select(x => x.CID).Single();
            var customer = this.customerRepo.QueryAll().Single(x => x.CID == cid);

            return customer;
        }
    }
}
