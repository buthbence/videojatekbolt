﻿// <copyright file="LoginControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL;
    using DAL.Intefaces;
    using DAL.Repos;
    using Rework;

    /// <summary>
    /// BLL class for user login and authentication.
    /// </summary>
    public class LoginControl
    {
        private IEMPLOYEERepository empRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginControl"/> class.
        /// </summary>
        /// <param name="empRepo">Repository pattern object used by the LoginControl.</param>
        public LoginControl(IEMPLOYEERepository empRepo)
        {
            this.empRepo = empRepo;
        }

        /// <summary>
        /// Checks whether a user with the given username and password exists within the system.
        /// </summary>
        /// <param name="uname">The entered username.</param>
        /// <param name="password">The entered password.</param>
        /// <returns>True if both parameters match a user, false otherwise.</returns>
        public EMPLOYEE Authenticate(string uname, string password)
        {
            var user = (from akt in this.empRepo.QueryAll()
                       where akt.UNAME == uname && akt.ACTIVE
                       select akt).SingleOrDefault();

            if (user != null && user.PWORD.ToLower() == password.ToSHA(Crypto.SHA_Type.SHA256).ToLower())
            {
                return user;
            }

            return null;
        }

        /// <summary>
        /// Checks wheter the user has to change their password on login or not.
        /// </summary>
        /// <param name="user">The user to check.</param>
        /// <returns>Returns true if the user has to change their password, false otherwise.</returns>
        public bool IsFirstTime(EMPLOYEE user)
        {
            return user.PASSRESET == null ? false : user.PASSRESET.Value;
        }

        /// <summary>
        /// Updates the given user's password if both the user entered the new password correctly twice.
        /// </summary>
        /// <param name="user">The user to update.</param>
        /// <param name="password">The entered new password.</param>
        /// <param name="passwordConfirmation">The confirmation of the new password.</param>
        /// <returns>A value indicating whether the user managed to change their password or not.</returns>
        public bool UpdatePassword(EMPLOYEE user, string password, string passwordConfirmation)
        {
            if (password == passwordConfirmation)
            {
                this.empRepo.ChangePassword(user.EID, password.ToSHA(Crypto.SHA_Type.SHA256));
                this.empRepo.ChangeResetPasswordStatus(user.EID);
                return true;
            }

            return false;
        }
    }
}
