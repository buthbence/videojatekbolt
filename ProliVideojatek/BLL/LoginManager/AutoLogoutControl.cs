﻿// <copyright file="AutoLogoutControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Threading;

    /// <summary>
    /// Class used to automatically log the user out if there was no action for 5 minutes.
    /// </summary>
    public class AutoLogoutControl
    {
        private const int INACTIVITYTIME = 300000;

        private Thread thread;
        private Stopwatch elapsedTime;
        private Action logoutMethods;
        private TaskScheduler scheduler = TaskScheduler.FromCurrentSynchronizationContext();

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoLogoutControl"/> class.
        /// </summary>
        /// <param name="logoutMethod">The method called when timer runs out.</param>
        public AutoLogoutControl(Action logoutMethod)
        {
            this.logoutMethods += logoutMethod;
            this.elapsedTime = new Stopwatch();
            this.elapsedTime.Start();
        }

        /// <summary>
        /// Resets timer when any action happens.
        /// </summary>
        public void ResetTimer()
        {
            this.elapsedTime.Restart();
        }

        /// <summary>
        /// Resets timer and starts a new thread that checks the timer.
        /// </summary>
        public void ResetTimerStartThread()
        {
            this.ResetTimer();
            this.thread = new Thread(this.Checktime);
            this.thread.IsBackground = true;
            this.thread.Start();
        }

        /// <summary>
        /// Checks whether the timer ran out or not. If it did, then it logs the user out.
        /// </summary>
        private void Checktime()
        {
            while (true)
            {
                if (this.elapsedTime.ElapsedMilliseconds > INACTIVITYTIME)
                {
                    using (Task t = new Task(this.logoutMethods))
                    {
                        t.Start(this.scheduler);
                        t.Wait();
                    }

                    this.thread.Join();
                }
            }
        }
    }
}
