﻿// <copyright file="CustomerControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Bll
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using DAL;
    using DAL.Intefaces;
    using DAL.Repos;

    /// <summary>
    /// BLL class for customer management.
    /// </summary>
    public class CustomerControl
    {
        private ICUSTOMERRepository customers;
        private ISOLDITEMRepository soldItems;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerControl"/> class.
        /// </summary>
        /// <param name="customers">Customer repository interface.</param>
        /// <param name="soldItems">Sold item repository interface.</param>
        public CustomerControl(ICUSTOMERRepository customers, ISOLDITEMRepository soldItems)
        {
            this.customers = customers;
            this.soldItems = soldItems;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerControl"/> class using <see cref="UniversalRepository"/>  .
        /// </summary>
        public CustomerControl()
        {
            this.customers = UniversalRepository.CustomerRepository;
            this.soldItems = UniversalRepository.SoldItemRepository;
        }

        /// <summary>
        /// Gets active customers from database.
        /// </summary>
        /// <returns>List of active customers</returns>
        public List<CUSTOMER> ActiveCustomers
        {
            get
            {
                var q = from item in this.customers.QueryAll()
                        where item.ACTIVE == true
                        select item;

                return q.ToList();
            }
        }

        /// <summary>
        /// Adds a new customer to the database
        /// </summary>
        /// <param name="newCustomer">The new customer to add</param>
        public void AddCustomer(CUSTOMER newCustomer)
        {
            newCustomer.CID = this.GetNewId();
            newCustomer.ACTIVE = true;
            UniversalRepository.CustomerRepository.Insert(newCustomer);
        }

        /// <summary>
        /// Removes customer from database.
        /// </summary>
        /// <param name="customer">Customer to remove.</param>
        public void RemoveCustomer(CUSTOMER customer)
        {
            this.customers.Remove(customer.CID);
        }

        /// <summary>
        /// Change a given customer's data in database.
        /// </summary>
        /// <param name="customer">Given customer.</param>
        public void ChangeCustomerData(CUSTOMER customer)
        {
            CUSTOMER customerInDatabase = (from item in this.customers.QueryAll()
                                          where item.CID == customer.CID
                                          select item).Single();

            if (customerInDatabase.CNAME != customer.CNAME)
            {
                this.customers.ChangeName(customer.CID, customer.CNAME);
            }

            if (customerInDatabase.EMAIL != customer.EMAIL)
            {
                this.customers.ChangeEmail(customer.CID, customer.EMAIL);
            }

            if (customerInDatabase.LCARD != customer.LCARD)
            {
                this.customers.ChangeLoyaltyCardStatus(customer.CID);
            }

            if (customerInDatabase.PHONE != customer.PHONE)
            {
                this.customers.ChangePhone(customer.CID, customer.PHONE);
            }

            if (customerInDatabase.ADDR != customer.ADDR)
            {
                this.customers.ChangeAddres(customer.CID, customer.ADDR);
            }
        }

        /// <summary>
        /// Returns customers filtered using a search term.
        /// </summary>
        /// <param name="searchTerm">Search keyword.</param>
        /// <returns>List of filtered customers.</returns>
        public List<CUSTOMER> FilterCustomers(string searchTerm)
        {
            var active = from item in this.customers.QueryAll()
                         where item.ACTIVE == true
                         select item;

            var result = from item in active
                         where item.CNAME.Contains(searchTerm) || item.EMAIL.Contains(searchTerm) || item.CID.ToString().Contains(searchTerm)
                         select item;

            return result.ToList();
        }

        /// <summary>
        /// Gets solditem elements from database for one customer.
        /// </summary>
        /// <param name="id">Customers id.</param>
        /// <returns>List of sold items.</returns>
        public List<SOLDITEM> GetSoldItemsByCustomerId(decimal id)
        {
            var result = from item in this.soldItems.QueryAll()
                         where item.SALE.CID == id && item.SALE.RESERVED == false
                         orderby item.SALE.DATE
                         select item;

            return result.ToList();
        }

        private decimal GetNewId()
        {
            var ids = from x in UniversalRepository.CustomerRepository.QueryAll()
                      select x.CID;
            decimal maxId = ids.Count() == 0 ? 0 : ids.Max();
            return maxId + 1;
        }
    }
}
