﻿// <copyright file="EmployeeControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Bll
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using DAL;
    using DAL.Intefaces;
    using DAL.Repos;
    using Rework;

    /// <summary>
    /// BLL class for user management.
    /// </summary>
    public class EmployeeControl
    {
        private static Random rnd = new Random();
        private IEMPLOYEERepository empRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeControl"/> class.
        /// </summary>
        /// <param name="empRepo">Repository pattern object used by the employee control.</param>
        public EmployeeControl(IEMPLOYEERepository empRepo)
        {
            this.empRepo = empRepo;
        }

        /// <summary>
        /// Gets the next available id in the user database.
        /// </summary>
        /// <returns>The available id.</returns>
        public decimal GetNewId
        {
            get
            {
                decimal result = this.empRepo.QueryAll().Max(x => x.EID) + 1;
                return result;
            }
        }

        /// <summary>
        /// Creates a username from the user's name.
        /// </summary>
        /// <param name="name">The user's name.</param>
        /// <returns>The created username.</returns>
        public static string CreateUsernameFromName(string name)
        {
            string username = name.Replace(' ', '.').ToLower();
            List<char> allowedCharacters = new List<char>();
            for (char i = 'a'; i < 'z' + 1; i++)
            {
                allowedCharacters.Add(i);
            }

            for (char i = '0'; i <= '9'; i++)
            {
                allowedCharacters.Add(i);
            }

            allowedCharacters.Add('.');

            Dictionary<char, char> accents = new Dictionary<char, char>();
            accents.Add('á', 'a');
            accents.Add('é', 'e');
            accents.Add('í', 'i');
            accents.Add('ö', 'o');
            accents.Add('ő', 'o');
            accents.Add('ú', 'u');
            accents.Add('ü', 'u');
            accents.Add('ű', 'u');

            string fixedUsername = string.Empty;

            for (int i = 0; i < username.Length; i++)
            {
                char currentChar = username[i];
                if (!allowedCharacters.Contains(currentChar))
                {
                    if (accents.ContainsKey(currentChar))
                    {
                        fixedUsername += accents[currentChar];
                    }
                    else
                    {
                        fixedUsername += "_";
                    }
                }
                else
                {
                    fixedUsername += currentChar;
                }
            }

            return fixedUsername;
        }

        /// <summary>
        /// Gets a filtered list of the employees.
        /// </summary>
        /// <param name="searchTerm">The entered search term.</param>
        /// <returns>The filtered list of employees.</returns>
        public List<EMPLOYEE> GetEmployees(string searchTerm)
        {
            var filteredEmployees = from akt in this.empRepo.QueryAll()
                                    where akt.ACTIVE != false && (searchTerm == null ||
                                    (akt.EID.ToString().ToLower().Contains(searchTerm) ||
                                    akt.ENAME.ToLower().Contains(searchTerm) ||
                                    akt.UNAME.ToLower().Contains(searchTerm)))
                                    orderby akt.EID ascending
                                    select akt;
            return filteredEmployees.ToList();
        }

        /// <summary>
        /// Generates a 5 characters long random password.
        /// </summary>
        /// <returns>The generated password.</returns>
        public string GeneratePassword()
        {
            string password = string.Empty;
            for (int i = 0; i < 5; i++)
            {
                int typeOfChar = rnd.Next(0, 3);
                switch (typeOfChar)
                {
                    case 0:
                        password += (char)rnd.Next(49, 58);
                        break;
                    case 1:
                        password += (char)rnd.Next(65, 91);
                        break;
                    case 2:
                        password += (char)rnd.Next(97, 123);
                        break;
                }
            }

            return password;
        }

        /// <summary>
        /// Checks whether the database already has a user with the same username.
        /// </summary>
        /// <param name="username">The username to check.</param>
        /// <returns>True if it already exists, false otherwise.</returns>
        public bool IsNewUser(string username)
        {
            return this.empRepo.QueryAll().Where(x => x.UNAME == username && x.ACTIVE).Count() == 0;
        }

        /// <summary>
        /// Adds a number at the end of the current username indicating the number of users with the same username.
        /// This is used to prevent duplicate usernames.
        /// </summary>
        /// <param name="username">The username to change.</param>
        /// <returns>The username with a number at the end.</returns>
        public string IncrementUsername(string username)
        {
            return username + (this.empRepo.QueryAll().Where(x => x.UNAME.StartsWith(username) && x.ACTIVE).Count() + 1);
        }

        /// <summary>
        /// Adds a new employee to the database.
        /// </summary>
        /// <param name="id">The new user's id.</param>
        /// <param name="name">The new user's name.</param>
        /// <param name="username">The new user's username.</param>
        /// <param name="password">The new user's password.</param>
        /// <param name="isAdmin">The new user's admin status.</param>
        public void AddNewEmployee(decimal id, string name, string username, string password, bool isAdmin)
        {
            this.empRepo.Insert(new EMPLOYEE
            {
                EID = id,
                ENAME = name,
                UNAME = username,
                PWORD = password.ToSHA(Crypto.SHA_Type.SHA256),
                PASSRESET = true,
                BOSS = isAdmin,
                ACTIVE = true
            });
        }

        /// <summary>
        /// Edits the user with the given id.
        /// </summary>
        /// <param name="id">The user's id.</param>
        /// <param name="name">The user's new name.</param>
        /// <param name="username">The user's new username.</param>
        /// <param name="password">The user's new password.</param>
        /// <param name="passReset">The user's new password reset status.</param>
        /// <param name="isAdmin">The user's new admin status.</param>
        public void EditEmployee(decimal id, string name, string username, string password, bool passReset, bool isAdmin)
        {
            this.empRepo.ChangeName(id, name);
            this.empRepo.ChangeUserName(id, username);

            if (passReset == true)
            {
                this.empRepo.ChangePassword(id, password.ToSHA(Crypto.SHA_Type.SHA256));
            }

            if (this.empRepo.QueryAll().Where(x => x.EID == id).Single().PASSRESET != passReset)
            {
                this.empRepo.ChangeResetPasswordStatus(id);
            }

            if (this.empRepo.QueryAll().Where(x => x.EID == id).Single().BOSS != isAdmin)
            {
                this.empRepo.ChangeBossStatus(id);
            }
        }

        /// <summary>
        /// Deletes the employee with the given id.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        public void DeleteEmployee(decimal id)
        {
            this.empRepo.Remove(id);
        }
    }
}
