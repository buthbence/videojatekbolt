﻿// <copyright file="AdminPageWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using DAL.Repos;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for AdminPageWindow.xaml
    /// </summary>
    public partial class AdminPageWindow : Window
    {
        private AdminPageWindowViewModel vM;
        private AdminPageWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminPageWindow"/> class.
        /// </summary>
        /// <param name="currentUser">The currently logged in user.</param>
        public AdminPageWindow(EMPLOYEE currentUser)
        {
            this.InitializeComponent();
            this.model = new AdminPageWindowModel() { EmployeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository) };
            this.model.SupplierControl = new SupplierControl();
            this.model.CurrentUser = currentUser;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vM = new AdminPageWindowViewModel();
            this.model.EmployeeSearchTerm = this.vM.EmployeeSearchTerm;
            this.vM.Employees = this.model.EmployeeControl.GetEmployees(this.model.EmployeeSearchTerm);
            this.model.Suppliers = this.model.SupplierControl.GetSuppliers();
            this.vM.Suppliers = new ObservableCollection<SUPPLIER>(this.model.Suppliers);
            this.DataContext = this.vM;
        }

        private void EmployeeAddButton_Click(object sender, RoutedEventArgs e)
        {
            bool? result = new NewEmployeeWindow().ShowDialog();
            if (result == null ? false : result.Value)
            {
                this.vM.Employees = this.model.EmployeeControl.GetEmployees(this.model.EmployeeSearchTerm);
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void EditSelectedEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.SelectedEmployee != this.model.CurrentUser)
            {
                this.model.SelectedUser = this.vM.SelectedEmployee;
                bool? result = new EditEmployeeWindow(this.model.SelectedUser).ShowDialog();
                if (result == null ? false : result.Value)
                {
                    this.vM.Employees = this.model.EmployeeControl.GetEmployees(this.model.EmployeeSearchTerm);
                }
            }
            else
            {
                MessageBox.Show("You cannot edit currently logged in user!");
            }
        }

        private void AddSupplier_Click(object sender, RoutedEventArgs e)
        {
            SUPPLIER newSupplier = new SUPPLIER();
            SupplierEditorWindow sew = new SupplierEditorWindow(newSupplier);
            sew.ShowDialog();
            if (sew.DialogResult == null ? false : sew.DialogResult.Value)
            {
                this.model.SupplierControl.AddNewSupplier(newSupplier);
                this.model.Suppliers.Add(newSupplier);
                this.vM.Suppliers.Add(newSupplier);
            }
        }

        private void EditSupplier_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.SelectedSupplier != null)
            {
                SupplierEditorWindow sew = new SupplierEditorWindow(this.vM.SelectedSupplier);
                sew.ShowDialog();
                if (sew.DialogResult != null ? sew.DialogResult.Value : false)
                {
                    this.model.SupplierControl.EditSupplier(this.vM.SelectedSupplier);
                    this.model.Suppliers = this.model.SupplierControl.GetSuppliers();
                    this.vM.Suppliers = new ObservableCollection<SUPPLIER>(this.model.Suppliers);
                }
            }
        }

        private void DeleteSupplier_Click(object sender, RoutedEventArgs e)
        {
            this.model.SelectedSupplier = this.vM.SelectedSupplier;
            if (this.model.SelectedSupplier != null)
            {
                this.model.SupplierControl.DeleteSupplier(this.model.SelectedSupplier);
                this.model.SupplierSearchTerm = this.vM.SupplierSearchTerm;
                this.model.Suppliers = this.model.SupplierControl.GetSuppliers(this.model.SupplierSearchTerm);
                this.vM.Suppliers = new ObservableCollection<SUPPLIER>(this.model.Suppliers);
            }
        }

        private void EmployeeSearch_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            this.model.EmployeeSearchTerm = this.vM.EmployeeSearchTerm;
            this.vM.Employees = this.model.EmployeeControl.GetEmployees(this.model.EmployeeSearchTerm);
        }

        private void SupplierSearch_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            this.model.SupplierSearchTerm = this.vM.SupplierSearchTerm;
            this.model.Suppliers = this.model.SupplierControl.GetSuppliers(this.model.SupplierSearchTerm);
            this.vM.Suppliers = new ObservableCollection<SUPPLIER>(this.model.Suppliers);
        }

        private void DeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.model.SelectedUser = this.vM.SelectedEmployee;
            if (this.model.SelectedUser != null)
            {
                if (this.DeleteEmployee(this.model.SelectedUser, this.model.CurrentUser))
                {
                    this.vM.Employees = this.model.EmployeeControl.GetEmployees(this.model.EmployeeSearchTerm);
                }
            }
        }

        private bool DeleteEmployee(EMPLOYEE selectedEmployee, EMPLOYEE currentUser)
        {
            if (selectedEmployee == currentUser)
            {
                MessageBox.Show("You cannot delete the currently logged in user!");
            }
            else
            {
                if (MessageBox.Show(
                    "Would you like to delete " + selectedEmployee.UNAME + "? This action cannot be undone!",
                    "Deleting user",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.model.EmployeeControl.DeleteEmployee(selectedEmployee.EID);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
