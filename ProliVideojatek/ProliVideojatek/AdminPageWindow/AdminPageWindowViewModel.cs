﻿// <copyright file="AdminPageWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DAL;

    /// <summary>
    /// The view model of the Admin Page Window.
    /// </summary>
    internal class AdminPageWindowViewModel : Bindable
    {
        private List<EMPLOYEE> employees;
        private EMPLOYEE selectedEmployee;
        private string employeeSearchTerm;

        private ObservableCollection<SUPPLIER> suppliers;
        private SUPPLIER selectedSupplier;
        private string supplierSearchTerm;

        /// <summary>
        /// Gets or sets the list of employees.
        /// </summary>
        public List<EMPLOYEE> Employees
        {
            get { return this.employees; }
            set { this.SetProperty(ref this.employees, value); }
        }

        /// <summary>
        /// Gets a value indicating whether a user has been selected from the employees list.
        /// </summary>
        public bool IsUserSelected
        {
            get { return this.selectedEmployee != null; }
        }

        /// <summary>
        /// Gets or sets the currently selected employee.
        /// </summary>
        public EMPLOYEE SelectedEmployee
        {
            get
            {
                return this.selectedEmployee;
            }

            set
            {
                this.SetProperty(ref this.selectedEmployee, value);
                this.OnPropertyChanged("IsUserSelected");
            }
        }

        /// <summary>
        /// Gets or sets the currently entered employee search term.
        /// </summary>
        public string EmployeeSearchTerm
        {
            get { return this.employeeSearchTerm; }
            set { this.SetProperty(ref this.employeeSearchTerm, value); }
        }

        /// <summary>
        /// Gets or sets the list of suppliers.
        /// </summary>
        public ObservableCollection<SUPPLIER> Suppliers
        {
            get { return this.suppliers; }
            set { this.SetProperty(ref this.suppliers, value); }
        }

        /// <summary>
        /// Gets or sets the currently selected supplier.
        /// </summary>
        public SUPPLIER SelectedSupplier
        {
            get
            {
                return this.selectedSupplier;
            }

            set
            {
                this.SetProperty(ref this.selectedSupplier, value);
                this.OnPropertyChanged("IsSupplierSelected");
            }
        }

        /// <summary>
        /// Gets or sets the currently entered search term for the suppliers list.
        /// </summary>
        public string SupplierSearchTerm
        {
            get { return this.supplierSearchTerm; }
            set { this.SetProperty(ref this.supplierSearchTerm, value); }
        }

        /// <summary>
        /// Gets a value indicating whether a supplier has been selected from the suppliers list.
        /// </summary>
        public bool IsSupplierSelected
        {
            get
            {
                return this.SelectedSupplier != null;
            }
        }
    }
}
