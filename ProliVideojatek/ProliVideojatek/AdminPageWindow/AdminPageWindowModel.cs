﻿// <copyright file="AdminPageWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Bll;
    using DAL;

    /// <summary>
    /// Model for the Admin Page Window.
    /// </summary>
    internal class AdminPageWindowModel
    {
        /// <summary>
        /// Gets or sets the BLL class for employee management.
        /// </summary>
        public EmployeeControl EmployeeControl { get; set; }

        /// <summary>
        /// Gets or sets the entered search term for the employees list.
        /// </summary>
        public string EmployeeSearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the currently selected user.
        /// </summary>
        public EMPLOYEE SelectedUser { get; set; }

        /// <summary>
        /// Gets or sets the entered search term for the suppliers list.
        /// </summary>
        public string SupplierSearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the list of suppliers.
        /// </summary>
        public List<SUPPLIER> Suppliers { get; set; }

        /// <summary>
        /// Gets or sets the currently selected supplier.
        /// </summary>
        public SUPPLIER SelectedSupplier { get; set; }

        /// <summary>
        /// Gets or sets the currently logged in user.
        /// </summary>
        public EMPLOYEE CurrentUser { get; set; }

        /// <summary>
        /// Gets or sets the supplier management business logic class.
        /// </summary>
        public SupplierControl SupplierControl { get; internal set; }
    }
}
