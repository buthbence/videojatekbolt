﻿// <copyright file="StockManagementWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using Bll;
    using DAL;
    using DAL.Repos;

    /// <summary>
    /// Model for the Stock Management window.
    /// </summary>
    internal class StockManagementWindowModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StockManagementWindowModel"/> class.
        /// </summary>
        public StockManagementWindowModel()
        {
            this.RestockControl = new ProductControl(
                UniversalRepository.ProductRepository,
                UniversalRepository.SupplierRepository,
                UniversalRepository.SupProdRepository,
                UniversalRepository.RestockRepository,
                UniversalRepository.CategoryRepository,
                UniversalRepository.SubCategoryRepository);
        }

        /// <summary>
        /// Gets or sets BLL used in the stock management window.
        /// </summary>
        public ProductControl RestockControl { get; set; }

        /// <summary>
        /// Gets or sets the list of restock requests.
        /// </summary>
        public List<RESTOCK> RestockList { get; set; }

        /// <summary>
        /// Gets or sets the list of items that we have marked as arrived.
        /// </summary>
        public List<RESTOCK> ArrivedProducts { get; set; }

        /// <summary>
        /// Gets or sets the searchterm entered into the searchbox.
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the selected restock request.
        /// </summary>
        public RESTOCK Selected { get; set; }
    }
}
