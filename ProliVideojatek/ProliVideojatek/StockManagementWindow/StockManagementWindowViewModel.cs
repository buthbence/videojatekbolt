﻿// <copyright file="StockManagementWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using DAL;

    /// <summary>
    /// View Model for the Stock Management window.
    /// </summary>
    internal class StockManagementWindowViewModel : Bindable
    {
        private List<RESTOCK> restockList;
        private string searchTerm;
        private RESTOCK selected;

        /// <summary>
        /// Gets or sets the list of restock requests.
        /// </summary>
        public List<RESTOCK> RestockList
        {
            get
            {
                return this.restockList;
            }

            set
            {
                this.SetProperty(ref this.restockList, value);
            }
        }

        /// <summary>
        /// Gets or sets the entered search term.
        /// </summary>
        public string SearchTerm
        {
            get
            {
                return this.searchTerm;
            }

            set
            {
                this.SetProperty(ref this.searchTerm, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected restock request.
        /// </summary>
        public RESTOCK Selected
        {
            get
            {
                return this.selected;
            }

            set
            {
                this.SetProperty(ref this.selected, value);
            }
        }
    }
}
