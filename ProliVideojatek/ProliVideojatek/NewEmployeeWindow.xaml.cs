﻿// <copyright file="NewEmployeeWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using Bll;
    using DAL.Repos;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for NewEmployeeWindow.xaml
    /// </summary>
    public partial class NewEmployeeWindow : Window
    {
        private NewEmployeeWindowViewModel vM;
        private NewEmployeeWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewEmployeeWindow"/> class.
        /// </summary>
        public NewEmployeeWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Called when the window is loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vM = new NewEmployeeWindowViewModel();
            this.model = new NewEmployeeWindowModel();
            this.model.EmployeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository);
            this.model.Id = this.model.EmployeeControl.GetNewId;
            this.model.Password = this.model.EmployeeControl.GeneratePassword();
            this.vM.Id = this.model.Id;
            this.vM.Password = this.model.Password;
            this.DataContext = this.vM;
        }

        /// <summary>
        /// Called when the user presses the back button to exit the window.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Called when the user presses the save button to save the changes.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.Name == string.Empty || this.vM.Name == null)
            {
                MessageBox.Show("Please enter a proper name!");
            }
            else
            {
                this.model.Username = this.vM.Username;
                this.model.Name = this.vM.Name;
                this.model.IsAdmin = this.vM.IsAdmin;
                if (this.model.EmployeeControl.IsNewUser(this.model.Username))
                {
                    this.DialogResult = true;
                    this.model.EmployeeControl.AddNewEmployee(this.model.Id, this.model.Name, this.model.Username, this.model.Password, this.model.IsAdmin);
                    this.Close();
                }
                else
                {
                    string newUsername = this.model.EmployeeControl.IncrementUsername(this.model.Username);
                    if (this.PreventUsernameCollision(this.model.Username, newUsername))
                    {
                        this.model.Username = newUsername;
                        this.model.EmployeeControl.AddNewEmployee(this.model.Id, this.model.Name, this.model.Username, this.model.Password, this.model.IsAdmin);
                        this.DialogResult = true;
                        this.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Asks the user if they want to prevent duplicate usernames using the built in function.
        /// </summary>
        /// <param name="username">The username the user wanted to use.</param>
        /// <param name="newUsername">The username the user would get.</param>
        /// <returns>Value indicating the user's choice.</returns>
        private bool PreventUsernameCollision(string username, string newUsername)
        {
            MessageBoxResult result = MessageBox.Show(
                    "'" + username + "' already exists in database. " +
                    "Would you like to save user as '" + newUsername + "' instead?", "Username already exists!",
                    MessageBoxButton.YesNo);
            return result == MessageBoxResult.Yes;
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
