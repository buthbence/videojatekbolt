﻿// <copyright file="SupProdBoolConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Converts SupProd's PID to a bool value indicating whether the given supplier supplies the product.
    /// </summary>
    public class SupProdBoolConverter : IValueConverter
    {
        /// <summary>
        /// Converts SupProd's PID to a bool value indicating whether the given supplier supplies the product.
        /// </summary>
        /// <param name="value">The supprod's PID.</param>
        /// <param name="targetType">The parameter is not used.</param>
        /// <param name="parameter">The parameter is not used.</param>
        /// <param name="culture">The parameter is not used.</param>
        /// <returns>The bool value indicating whether the given supplier supplies the product.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal pid = (decimal)value;
            return pid > 0 ? true : false;
        }

        /// <summary>
        /// The method is not used.
        /// </summary>
        /// <param name="value">The parameter is not used.</param>
        /// <param name="targetType">The parameter is not used.</param>
        /// <param name="parameter">The parameter is not used.</param>
        /// <param name="culture">The parameter is not used.</param>
        /// <returns>Does not return anything because it is not used.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
