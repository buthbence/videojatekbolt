﻿// <copyright file="CustomersWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using Bll;
    using DAL;

    /// <summary>
    /// Model for CustomersWindow
    /// </summary>
    internal class CustomersWindowModel
    {
        /// <summary>
        /// Gets or sets selected customer.
        /// </summary>
        public CUSTOMER SelectedCustomer { get; set; }

        /// <summary>
        /// Gets or sets Listof customers.
        /// </summary>
        public List<CUSTOMER> Customers { get; set; }

        /// <summary>
        /// Gets or sets seartch term used to filter customers.
        /// </summary>
        public string CustomerSearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the BLL object for the customers window.
        /// </summary>
        public CustomerControl CustomerControl { get; set; }
    }
}
