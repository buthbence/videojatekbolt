﻿// <copyright file="CustomersWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using DAL;

    /// <summary>
    /// View mode fro CustomersWindow.
    /// </summary>
    internal class CustomersWindowViewModel : Bindable
    {
        private List<CUSTOMER> customers;
        private CUSTOMER selectedCustomer;
        private string customerSearchTerm;

        /// <summary>
        /// Gets or sets list of customers.
        /// </summary>
        public List<CUSTOMER> Customers
        {
            get
            {
                return this.customers;
            }

            set
            {
                this.SetProperty(ref this.customers, value);
            }
        }

        /// <summary>
        /// Gets or sets selected customer.
        /// </summary>
        public CUSTOMER SelectedCustomer
        {
            get
            {
                return this.selectedCustomer;
            }

            set
            {
                this.SetProperty(ref this.selectedCustomer, value);
            }
        }

        /// <summary>
        /// Gets or sets customer search term.
        /// </summary>
        public string CustomerSearchTerm
        {
            get
            {
                return this.customerSearchTerm;
            }

            set
            {
                this.SetProperty(ref this.customerSearchTerm, value);
            }
        }
    }
}
