﻿// <copyright file="EditEmployeeWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using Bll;

    /// <summary>
    /// The model for the Employee Editor window.
    /// </summary>
    internal class EditEmployeeWindowModel
    {
        /// <summary>
        /// Gets or sets the value of the BLL class for editing users.
        /// </summary>
        public EmployeeControl EmployeeControl { get; set; }

        /// <summary>
        /// Gets or sets the currently selected user's ID.
        /// </summary>
        public decimal Id { get; set; }

        /// <summary>
        /// Gets or sets the currently selected user's currently set name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the currently selected user's old name.
        /// </summary>
        public string OldName { get; set; }

        /// <summary>
        /// Gets or sets the currently selected user's currently set username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the currently selected user's old username.
        /// </summary>
        public string OldUsername { get; set; }

        /// <summary>
        /// Gets or sets the currently selected user's password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user's password will have to be changed on next logon.
        /// </summary>
        public bool PassReset { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has admin rights.
        /// </summary>
        public bool IsAdmin { get; set; }
    }
}
