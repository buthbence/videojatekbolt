﻿// <copyright file="ProductMenuWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using DAL;

    /// <summary>
    /// Viewmodel class for the Product Menu window
    /// </summary>
    internal class ProductMenuWindowViewModel : Bindable
    {
        private List<PRODUCT> products;
        private List<CATEGORY> categories;
        private List<SUB_CATEGORY> subCategories;
        private Dictionary<PRODUCT, int> cart;
        private List<PRODUCT> oftenBoughtWith;
        private string addToCartQuantity;
        private string selectedProductInCartQuantity;
        private string productSearchTerm;
        private int setMinPrice;
        private int setMaxPrice;
        private int productsMinPrice;
        private int productsMaxPrice;
        private bool onlyAvailableItems;
        private CATEGORY selectedCategory;
        private SUB_CATEGORY selectedSubCategory;
        private bool isBoss;
        private PRODUCT selectedProduct;
        private PRODUCT oftenBoughtWithSelectedProduct;
        private KeyValuePair<PRODUCT, int> selectedCartItem;
        private string totalPrice;

        /// <summary>
        /// Gets or sets the products list (calls PropertyChanged when setting)
        /// </summary>
        public List<PRODUCT> Products
        {
            get
            {
                return this.products;
            }

            set
            {
                this.SetProperty(ref this.products, value);
            }
        }

        /// <summary>
        /// Gets or sets the product search term (calls PropertyChanged when setting)
        /// </summary>
        public string ProductSearchTerm
        {
            get
            {
                return this.productSearchTerm;
            }

            set
            {
                this.SetProperty(ref this.productSearchTerm, value);
            }
        }

        /// <summary>
        /// Gets or sets the minimunm price (calls PropertyChanged when setting)
        /// </summary>
        public int SetMinPrice
        {
            get
            {
                return this.setMinPrice;
            }

            set
            {
                this.SetProperty(ref this.setMinPrice, value);
            }
        }

        /// <summary>
        /// Gets or sets the maximum price (calls PropertyChanged when setting)
        /// </summary>
        public int SetMaxPrice
        {
            get
            {
                return this.setMaxPrice;
            }

            set
            {
                this.SetProperty(ref this.setMaxPrice, value);
            }
        }

        /// <summary>
        /// Gets or sets the products minimum price (calls PropertyChanged when setting)
        /// </summary>
        public int ProductsMinPrice
        {
            get
            {
                return this.productsMinPrice;
            }

            set
            {
                this.SetProperty(ref this.productsMinPrice, value);
            }
        }

        /// <summary>
        /// Gets or sets the products maximum price (calls PropertyChanged when setting)
        /// </summary>
        public int ProductsMaxPrice
        {
            get
            {
                return this.productsMaxPrice;
            }

            set
            {
                this.SetProperty(ref this.productsMaxPrice, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user checked the Only available items checkbox.
        /// </summary>
        public bool OnlyAvailableItems
        {
            get
            {
                return this.onlyAvailableItems;
            }

            set
            {
                this.SetProperty(ref this.onlyAvailableItems, value);
            }
        }

        /// <summary>
        /// Gets or sets the categories list (calls PropertyChanged when setting)
        /// </summary>
        public List<CATEGORY> Categories
        {
            get
            {
                return this.categories;
            }

            set
            {
                this.SetProperty(ref this.categories, value);
            }
        }

        /// <summary>
        /// Gets or sets the subcategories list (calls PropertyChanged when setting)
        /// </summary>
        public List<SUB_CATEGORY> SubCategories
        {
            get
            {
                return this.subCategories;
            }

            set
            {
                this.SetProperty(ref this.subCategories, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected category (calls PropertyChanged when setting)
        /// </summary>
        public CATEGORY SelectedCategory
        {
            get
            {
                return this.selectedCategory;
            }

            set
            {
                this.SetProperty(ref this.selectedCategory, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected subcategory (calls PropertyChanged when setting)
        /// </summary>
        public SUB_CATEGORY SelectedSubCategory
        {
            get
            {
                return this.selectedSubCategory;
            }

            set
            {
                this.SetProperty(ref this.selectedSubCategory, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the current user is a boss.
        /// </summary>
        public bool IsBoss
        {
            get
            {
                return this.isBoss;
            }

            set
            {
                this.SetProperty(ref this.isBoss, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected product (calls PropertyChanged when setting)
        /// </summary>
        public PRODUCT SelectedProduct
        {
            get
            {
                return this.selectedProduct;
            }

            set
            {
                this.SetProperty(ref this.selectedProduct, value);
            }
        }

        /// <summary>
        /// Gets or sets the cart. (calls PropertyChanged when setting)
        /// </summary>
        public Dictionary<PRODUCT, int> Cart
        {
            get
            {
                return this.cart;
            }

            set
            {
                this.SetProperty(ref this.cart, value);
            }
        }

        /// <summary>
        /// Gets or sets the quantity to adding an item to the cart (calls PropertyChanged when setting)
        /// </summary>
        public string AddToCartQuantity
        {
            get
            {
                return this.addToCartQuantity;
            }

            set
            {
                this.SetProperty(ref this.addToCartQuantity, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected products quantity in the cart (calls PropertyChanged when setting)
        /// </summary>
        public string SelectedProductInCartQuantity
        {
            get
            {
                return this.selectedProductInCartQuantity;
            }

            set
            {
                this.SetProperty(ref this.selectedProductInCartQuantity, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected cart item, a product with quantity (calls PropertyChanged when setting)
        /// </summary>
        public KeyValuePair<PRODUCT, int> SelectedCartItem
        {
            get
            {
                return this.selectedCartItem;
            }

            set
            {
                this.SetProperty(ref this.selectedCartItem, value);
            }
        }

        /// <summary>
        /// Gets or sets the carts total price (calls PropertyChanged when setting)
        /// </summary>
        public string TotalPrice
        {
            get
            {
                return this.totalPrice;
            }

            set
            {
                this.SetProperty(ref this.totalPrice, value);
            }
        }

        /// <summary>
        /// Gets or sets the often bought together products list (calls PropertyChanged when setting)
        /// </summary>
        public List<PRODUCT> OftenBoughtWith
        {
            get
            {
                return this.oftenBoughtWith;
            }

            set
            {
                this.SetProperty(ref this.oftenBoughtWith, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected product int the "often bought together" list (calls PropertyChanged when setting)
        /// </summary>
        public PRODUCT OftenBoughtWithSelectedProduct
        {
            get
            {
                return this.oftenBoughtWithSelectedProduct;
            }

            set
            {
                this.SetProperty(ref this.oftenBoughtWithSelectedProduct, value);
            }
        }
    }
}
