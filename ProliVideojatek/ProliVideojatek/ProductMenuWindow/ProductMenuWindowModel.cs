﻿// <copyright file="ProductMenuWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using Bll;
    using DAL;

    /// <summary>
    /// Model class for Product Menu window
    /// </summary>
    internal class ProductMenuWindowModel
    {
        /// <summary>
        /// Gets or sets the business logic class SalesControl object
        /// </summary>
        public SalesControl SalesControl { get; internal set; }

        /// <summary>
        /// Gets or sets the currently logged in user
        /// </summary>
        public EMPLOYEE User { get; set; }

        /// <summary>
        /// Gets or sets the cart.
        /// </summary>
        public Dictionary<PRODUCT, int> Cart { get; set; }

        /// <summary>
        /// Gets or sets the often bought together product list
        /// </summary>
        public List<PRODUCT> OftenBoughtWith { get; set; }

        /// <summary>
        /// Gets or sets the quantity of adding an item into the cart
        /// </summary>
        public int AddToCartQuantity { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the selected product in the cart
        /// </summary>
        public int SelectedProductInCartQuantity { get; set; }

        /// <summary>
        /// Gets or sets the selected product in the cart
        /// </summary>
        public PRODUCT SelectedProductInCart { get; set; }

        /// <summary>
        /// Gets or sets the search term for filtering products
        /// </summary>
        public string ProductSearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the setMinPrice field.
        /// </summary>
        public int SetMinPrice { get; set; }

        /// <summary>
        /// Gets or sets the setMaxPrice field.
        /// </summary>
        public int SetMaxPrice { get; set; }

        /// <summary>
        /// Gets or sets the products mint price filter.
        /// </summary>
        public int ProductsMinPrice { get; set; }

        /// <summary>
        /// Gets or sets the products max price filter.
        /// </summary>
        public int ProductsMaxPrice { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user would like to see only available product.
        /// </summary>
        public bool OnlyAvailableItems { get; set; }

        /// <summary>
        /// Gets or sets the selected category.
        /// </summary>
        public CATEGORY SelectedCategory { get; set; }

        /// <summary>
        /// Gets or sets the selected subcategory.
        /// </summary>
        public SUB_CATEGORY SelectedSubCategory { get; set; }

        /// <summary>
        /// Gets or sets the selected product
        /// </summary>
        public PRODUCT SelectedProduct { get; set; }

        /// <summary>
        /// Gets or sets the selected product of the often bought together list.
        /// </summary>
        public PRODUCT OftenBoughtWithSelectedProduct { get; set; }

        /// <summary>
        /// Gets or sets the business logic class ProductControl object
        /// </summary>
        public ProductControl ProductControl { get; set; }

        /// <summary>
        /// Gets or sets the carts total price.
        /// </summary>
        public int CartTotalPrice { get; set; }

        /// <summary>
        /// Gets or sets the carts total price for loyal customers
        /// </summary>
        public int CartTotalProceForLoyals { get; set; }

        /// <summary>
        /// Calculates the cart's total prices (with or without loyality card prices).
        /// </summary>
        internal void CalculateTotalPrices()
        {
            this.CartTotalPrice = this.SalesControl.CartTotalPrice(this.Cart, false);
            this.CartTotalProceForLoyals = this.SalesControl.CartTotalPrice(this.Cart, true);
        }
    }
}
