﻿// <copyright file="PurchaseWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using Bll;
    using DAL;

    /// <summary>
    /// Model for purchase window
    /// </summary>
    internal class PurchaseWindowModel
    {
        /// <summary>
        /// Gets or sets the cart
        /// </summary>
        public Dictionary<PRODUCT, int> Cart { get; set; }

        /// <summary>
        /// Gets or sets the list of customers
        /// </summary>
        public List<CUSTOMER> Customers { get; set; }

        /// <summary>
        /// Gets or sets the selected customer
        /// </summary>
        public CUSTOMER SelectedCustomer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current purchase is a reservation or not.
        /// </summary>
        public bool IsReservation { get; set; }

        /// <summary>
        /// Gets or sets the total price.
        /// </summary>
        public int TotalPrice { get; internal set; }

        /// <summary>
        /// Gets or sets the customer control business logic class object.
        /// </summary>
        public CustomerControl CustomerControl { get; internal set; }

        /// <summary>
        /// Gets or sets the current employee
        /// </summary>
        public EMPLOYEE User { get; internal set; }
    }
}
