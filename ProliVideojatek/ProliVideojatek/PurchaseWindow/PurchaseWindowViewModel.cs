﻿// <copyright file="PurchaseWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using DAL;

    /// <summary>
    /// Viewmodel class for the purchase window
    /// </summary>
    internal class PurchaseWindowViewModel : Bindable
    {
        private ObservableCollection<CUSTOMER> customers;
        private Dictionary<PRODUCT, int> cart;
        private CUSTOMER selectedCustomer;
        private bool isReservation;
        private string totalPrice;

        /// <summary>
        /// Gets or sets the Customers list and calls PropertyChanged
        /// </summary>
        public ObservableCollection<CUSTOMER> Customers
        {
            get
            {
                return this.customers;
            }

            set
            {
                this.SetProperty(ref this.customers, value);
            }
        }

        /// <summary>
        /// Gets or sets the cart and calls PropertyChanged
        /// </summary>
        public Dictionary<PRODUCT, int> Cart
        {
            get
            {
                return this.cart;
            }

            set
            {
                this.SetProperty(ref this.cart, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected customer and calls PropertyChanged
        /// </summary>
        public CUSTOMER SelectedCustomer
        {
            get
            {
                return this.selectedCustomer;
            }

            set
            {
                this.SetProperty(ref this.selectedCustomer, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the purchase is a reservation or not and calls PropertyChanged
        /// </summary>
        public bool IsReservation
        {
            get
            {
                return this.isReservation;
            }

            set
            {
                this.SetProperty(ref this.isReservation, value);
            }
        }

        /// <summary>
        /// Gets or sets the total price of the purhcase and calls PropertyChanged
        /// </summary>
        public string TotalPrice
        {
            get
            {
                return string.Format("Total price (with discounts): {0} Ft", this.totalPrice);
            }

            set
            {
                this.SetProperty(ref this.totalPrice, value);
            }
        }
    }
}
