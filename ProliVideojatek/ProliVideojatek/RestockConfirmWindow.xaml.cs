﻿// <copyright file="RestockConfirmWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for RestockConfirmWindow.xaml
    /// </summary>
    public partial class RestockConfirmWindow : Window
    {
        private RestockConfirmWindowViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestockConfirmWindow"/> class.
        /// </summary>
        /// <param name="product">The product the user wants to restock.</param>
        /// <param name="supplier">The supplier that will do the restocking.</param>
        /// <param name="restock">The amount.</param>
        public RestockConfirmWindow(PRODUCT product, SUPPLIER supplier, int restock)
        {
            this.InitializeComponent();

            this.vm = new RestockConfirmWindowViewModel();
            this.vm.Product = product;
            this.vm.Supplier = supplier;
            this.vm.RestockAmount = restock;

            this.DataContext = this.vm;
        }

        /// <summary>
        /// Closes the window when the Cancel button is pressed.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Closes the window when the OK button is pressed.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
