﻿// <copyright file="EditProductWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using Bll;
    using DAL;

    /// <summary>
    /// Model class for Edit Product window
    /// </summary>
    internal class EditProductWindowModel
    {
        /// <summary>
        /// Gets or sets the product ID
        /// </summary>
        public decimal Pid { get; set; }

        /// <summary>
        /// Gets or sets the product name
        /// </summary>
        public string Pname { get; set; }

        /// <summary>
        /// Gets or sets the product's subcategory
        /// </summary>
        public decimal Psubcat { get; set; }

        /// <summary>
        /// Gets or sets the procuct's amount in stock
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// Gets or sets the product's price
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets the discount for the product
        /// </summary>
        public int Discount { get; set; }

        /// <summary>
        /// Gets or sets the restock amount
        /// </summary>
        public int RestockAmount { get; set; }

        /// <summary>
        /// Gets or sets the selected supplier
        /// </summary>
        public SUPPROD SelectedSupplier { get; set; }

        /// <summary>
        /// Gets or sets the selected subcategory
        /// </summary>
        public SUB_CATEGORY SelectedSubCategory { get; set; }

        /// <summary>
        /// Gets or sets the selected category
        /// </summary>
        public CATEGORY SelectedCategory { get; set; }

        /// <summary>
        /// Gets or sets the list of categories
        /// </summary>
        public List<CATEGORY> Categories { get; set; }

        /// <summary>
        /// Gets or sets the list of the subcategories
        /// </summary>
        public List<SUB_CATEGORY> SubCategories { get; set; }

        /// <summary>
        /// Gets or sets the list of suppliers
        /// </summary>
        public List<SUPPROD> Suppliers { get; set; }

        /// <summary>
        /// Gets or sets the business logic class ProductControl instance
        /// </summary>
        public ProductControl ProductControl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the logged user is a simple user.
        /// </summary>
        public bool IsUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the logged user is an administrator.
        /// </summary>
        public bool IsAdmin { get; set; }
    }
}
