﻿// <copyright file="EditProductWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using DAL;

    /// <summary>
    /// ViewModel class for Edit Product window
    /// </summary>
    internal class EditProductWindowViewModel : Bindable
    {
        private string searchTerm;
        private SUB_CATEGORY selectedSubCategory;
        private CATEGORY selectedCategory;
        private List<CATEGORY> categories;
        private List<SUB_CATEGORY> subCategories;
        private int selectedSubCategoryIndex;
        private int selectedCategoryIndex;
        private SUPPROD selectedSupplier;
        private string windowTitle;

        /// <summary>
        /// Gets or sets a value indicating whether the logged user is an administrator.
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the logged user is a simple user.
        /// </summary>
        public bool IsUser { get; set; }

        /// <summary>
        /// Gets or sets the rastock amount
        /// </summary>
        public int RestockAmount { get; set; }

        /// <summary>
        /// Gets or sets the selected subcategory (calls PropertyChanged on set)
        /// </summary>
        public SUB_CATEGORY SelectedSubCategory
        {
            get
            {
                return this.selectedSubCategory;
            }

            set
            {
                this.SetProperty(ref this.selectedSubCategory, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected category (calls PropertyChanged on set)
        /// </summary>
        public CATEGORY SelectedCategory
        {
            get
            {
                return this.selectedCategory;
            }

            set
            {
                this.SetProperty(ref this.selectedCategory, value);
            }
        }

        /// <summary>
        /// Gets or sets the list of categories (calls PropertyChanged on set)
        /// </summary>
        public List<CATEGORY> Categories
        {
            get
            {
                return this.categories;
            }

            set
            {
                this.SetProperty(ref this.categories, value);
            }
        }

        /// <summary>
        /// Gets or sets the list of subcategories (calls PropertyChanged on set)
        /// </summary>
        public List<SUB_CATEGORY> SubCategories
        {
            get
            {
                return this.subCategories;
            }

            set
            {
                this.SetProperty(ref this.subCategories, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected subcategory index (calls PropertyChanged on set)
        /// </summary>
        public int SelectedSubCategoryIndex
        {
            get
            {
                return this.selectedSubCategoryIndex;
            }

            set
            {
                this.SetProperty(ref this.selectedSubCategoryIndex, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected category index (calls PropertyChanged on set)
        /// </summary>
        public int SelectedCategoryIndex
        {
            get
            {
                return this.selectedCategoryIndex;
            }

            set
            {
                this.SetProperty(ref this.selectedCategoryIndex, value);
            }
        }

        /// <summary>
        /// Gets or sets the search term (calls PropertyChanged on set)
        /// </summary>
        public string SearchTerm
        {
            get
            {
                return this.searchTerm;
            }

            set
            {
                this.SetProperty(ref this.searchTerm, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected supplier (calls PropertyChanged on set)
        /// </summary>
        public SUPPROD SelectedSupplier
        {
            get
            {
                return this.selectedSupplier;
            }

            set
            {
                this.SetProperty(ref this.selectedSupplier, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the window is for editing or for adding a new item
        /// </summary>
        public bool IsEditWindow { get; set; }

        /// <summary>
        /// Gets or sets the product id
        /// </summary>
        public decimal Pid { get; set; }

        /// <summary>
        /// Gets or sets the product name
        /// </summary>
        public string Pname { get; set; }

        /// <summary>
        /// Gets or sets the products subcategory
        /// </summary>
        public decimal Psubcat { get; set; }

        /// <summary>
        /// Gets or sets the products stock quantity
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// Gets or sets the products price
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets the amount of discount on the product
        /// </summary>
        public int Discount { get; set; }

        /// <summary>
        /// Gets or sets the list of suppliers
        /// </summary>
        public List<SUPPROD> Suppliers { get; set; }

        /// <summary>
        /// Gets or sets the window title
        /// </summary>
        public string WindowTitle
        {
            get
            {
                return this.windowTitle;
            }

            set
            {
                this.SetProperty(ref this.windowTitle, value);
            }
        }
    }
}
