﻿// <copyright file="EditProductWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for EditProductWindow.xaml
    /// </summary>
    public partial class EditProductWindow : Window
    {
        private EditProductWindowModel model;

        private EditProductWindowViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditProductWindow"/> class.
        /// </summary>
        /// <param name="product">The product to edit.</param>
        /// <param name="edit">Datermines that the window is for edit a product, or to create a new product.</param>
        /// /// <param name="user">Logged in user.</param>
        public EditProductWindow(PRODUCT product, bool edit, EMPLOYEE user)
        {
            this.InitializeComponent();

            this.model = new EditProductWindowModel();
            this.vM = new EditProductWindowViewModel();
            this.model.ProductControl = new ProductControl();
            this.model.Pid = product != null ? product.PID : this.model.ProductControl.NewProductId;
            this.model.Pname = product != null ? product.PNAME : string.Empty;
            this.model.Price = product != null ? (int)product.PRICE : 0;
            this.model.Stock = product != null ? (int)product.STOCK : 0;
            if (product != null)
            {
                this.model.Psubcat = product.PSUBCAT;
            }

            this.model.Suppliers = product != null ? this.model.ProductControl.GetSuppliers(product.PID) : this.model.ProductControl.GetSuppliers();

            this.model.Categories = this.model.ProductControl.Categories;
            this.model.Categories.RemoveAll(x => x.CAT_NAME == "Any");
            this.model.SubCategories = product != null ? this.model.ProductControl.GetSubcategories(this.model.Categories.Single(x => x.CAT_ID == product.SUB_CATEGORY.CATEGORY.CAT_ID)) : new List<SUB_CATEGORY>();
            this.model.SubCategories.RemoveAll(x => x.SUB_CAT_NAME == "Any");
            this.model.SelectedCategory = product != null ? this.model.Categories.Single(x => x.CAT_ID == product.SUB_CATEGORY.CATEGORY.CAT_ID) : null;
            this.model.SelectedSubCategory = product != null ? this.model.SubCategories.Single(x => x.SUB_CAT_ID == product.SUB_CATEGORY.SUB_CAT_ID) : null;
            this.model.Discount = product != null && product.DISCOUNT != null ? (int)product.DISCOUNT : 0;

            this.vM.Pid = this.model.Pid;
            this.vM.Pname = this.model.Pname;
            this.vM.Price = this.model.Price;
            this.vM.Stock = this.model.Stock;
            this.vM.Categories = this.model.Categories;
            this.vM.SubCategories = this.model.SubCategories;
            this.vM.SelectedCategory = this.model.SelectedCategory;
            this.vM.SelectedSubCategory = this.model.SelectedSubCategory;
            this.vM.Suppliers = this.model.Suppliers;
            this.vM.Discount = this.model.Discount;

            if (product != null)
            {
                this.vM.Psubcat = product.SUB_CATEGORY.SUB_CAT_ID;
            }

            this.vM.IsEditWindow = edit;

            this.model.IsUser = !(bool)user.BOSS;
            this.vM.IsUser = this.model.IsUser;
            this.model.IsAdmin = (bool)user.BOSS;
            this.vM.IsAdmin = this.model.IsAdmin;

            this.DataContext = this.vM;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.SelectedCategory == null)
            {
                MessageBox.Show("Please select category!");
            }
            else if (string.IsNullOrEmpty(this.vM.Pname))
            {
                MessageBox.Show("Please Input Name");
            }
            else
            {
                this.model.SelectedSubCategory = this.vM.SelectedSubCategory;
                this.model.Pname = this.vM.Pname;
                this.model.Price = this.vM.Price;
                this.model.Stock = this.vM.Stock;
                this.model.Discount = this.vM.Discount;
                this.model.Suppliers = this.vM.Suppliers;

                PRODUCT p = new PRODUCT()
                {
                    PID = this.model.Pid,
                    PNAME = this.model.Pname,
                    PRICE = this.model.Price,
                    PSUBCAT = this.model.SelectedSubCategory.SUB_CAT_ID,
                    ACTIVE = true,
                    STOCK = this.model.Stock,
                    DISCOUNT = this.model.Discount
                };

                if (this.vM.IsEditWindow)
                {
                    this.model.ProductControl.ChangeProductData(p);
                }
                else
                {
                    this.model.ProductControl.AddNewProduct(p);
                }

                this.model.ProductControl.UpdateSupprodData(this.model.Pid, this.model.Suppliers);

                this.Close();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.model.SelectedCategory = this.vM.SelectedCategory;
            this.model.SubCategories = this.model.ProductControl.GetSubcategories(this.model.SelectedCategory);
            this.model.SubCategories.RemoveAll(x => x.SUB_CAT_NAME == "Any");
            this.model.SelectedSubCategory = this.model.SubCategories.Count(x => x.SUB_CAT_ID == this.vM.Psubcat) > 0 ? this.model.SubCategories.Single(x => x.SUB_CAT_ID == this.vM.Psubcat) : this.model.SubCategories.First();
            this.vM.SelectedSubCategory = this.model.SelectedSubCategory;
            this.vM.SubCategories = this.model.SubCategories;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char[] numbers = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            if (!numbers.Contains(e.Text[0]))
            {
                e.Handled = true;
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (!this.model.IsUser)
            {
                var baseobj = sender as FrameworkElement;
                var obj = baseobj.DataContext as SUPPROD;

                if (obj.PID > 0)
                {
                    obj.PID = 0;
                }
                else
                {
                    obj.PID = this.model.Pid;
                }
            }
        }

        private void RestockButton_Click(object sender, RoutedEventArgs e)
        {
            this.model.Suppliers = this.vM.Suppliers;
            this.model.ProductControl.UpdateSupprodData(this.model.Pid, this.model.Suppliers);
            if (this.vM.SelectedSupplier != null)
            {
                this.model.SelectedSupplier = this.vM.SelectedSupplier;
                this.vM.SelectedSupplier = this.model.Suppliers.Single(x => x.SID == this.vM.SelectedSupplier.SID);
            }

            this.model.SelectedSupplier = this.vM.SelectedSupplier;

            this.model.RestockAmount = this.vM.RestockAmount;
            if (this.model.SelectedSupplier == null)
            {
                MessageBox.Show("Please select a Supplier !");
            }
            else if (this.model.SelectedSupplier.PID == 0)
            {
                MessageBox.Show("Please select a valid Supplier !");
            }
            else if (this.model.RestockAmount <= 0)
            {
                MessageBox.Show("Please input a valid amount !");
            }
            else
            {
                RestockConfirmWindow w = new RestockConfirmWindow(new PRODUCT() { PID = this.model.Pid, PNAME = this.model.Pname }, this.model.SelectedSupplier.SUPPLIER, this.model.RestockAmount);
                w.ShowDialog();
                if (w.DialogResult == true)
                {
                    this.model.ProductControl.InsertRestock(this.model.Pid, this.model.SelectedSupplier.SID, this.model.RestockAmount);
                }
            }
        }

        private void AddNewSupplierButton_Click(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
