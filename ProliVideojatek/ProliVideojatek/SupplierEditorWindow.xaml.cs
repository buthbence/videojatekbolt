﻿// <copyright file="SupplierEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for SupplierEditor.xaml
    /// </summary>
    public partial class SupplierEditorWindow : Window
    {
        private SupplierEditorWindowViewModel viewModel;
        private SupplierEditorWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="SupplierEditorWindow"/> class to create a new supplier
        /// </summary>
        public SupplierEditorWindow()
        {
            this.InitializeComponent();
            this.model = new SupplierEditorWindowModel();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SupplierEditorWindow"/> class.
        /// </summary>
        /// <param name="supplier">The supplier to edit</param>
        public SupplierEditorWindow(SUPPLIER supplier)
            : this()
        {
            this.model.Supplier = supplier;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.model.Supplier.SNAME = this.viewModel.Name;
            this.model.Supplier.SEMAIL = this.viewModel.Email;
            this.model.Supplier.SPHONE = this.viewModel.Phone;
            if (string.IsNullOrEmpty(this.model.Supplier.SNAME) || string.IsNullOrWhiteSpace(this.model.Supplier.SNAME))
            {
                MessageBox.Show("Please enter a name!");
            }
            else if (string.IsNullOrEmpty(this.model.Supplier.SEMAIL) || string.IsNullOrWhiteSpace(this.model.Supplier.SEMAIL))
            {
                MessageBox.Show("Please enter an email!");
            }
            else if (string.IsNullOrEmpty(this.model.Supplier.SPHONE) || string.IsNullOrWhiteSpace(this.model.Supplier.SPHONE))
            {
                MessageBox.Show("Please enter a phone number!");
            }
            else
            {
                this.DialogResult = true;
                this.Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.model.Supplier != null)
            {
                this.viewModel = new SupplierEditorWindowViewModel()
                {
                    Id = this.model.Supplier.SID.ToString(),
                    Name = this.model.Supplier.SNAME,
                    Email = this.model.Supplier.SEMAIL,
                    Phone = this.model.Supplier.SPHONE
                };
            }
            else
            {
                this.viewModel = new SupplierEditorWindowViewModel();
            }

            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
