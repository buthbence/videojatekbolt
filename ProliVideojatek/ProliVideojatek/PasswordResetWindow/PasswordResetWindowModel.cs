﻿// <copyright file="PasswordResetWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using Bll;
    using DAL;

    /// <summary>
    /// Model for the Password Reset window.
    /// </summary>
    internal class PasswordResetWindowModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordResetWindowModel"/> class.
        /// </summary>
        /// <param name="user">The user whose password has to be reset.</param>
        public PasswordResetWindowModel(EMPLOYEE user)
        {
            this.User = user;
        }

        /// <summary>
        /// Gets or sets the BLL object used by the password reset window.
        /// </summary>
        public LoginControl LoginControl { get; set; }

        /// <summary>
        /// Gets or sets the entered password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the entered password confirmation.
        /// </summary>
        public string PasswordConfirmation { get; set; }

        /// <summary>
        /// Gets the user whose password is being changed.
        /// </summary>
        public EMPLOYEE User { get; private set; }
    }
}
