﻿// <copyright file="PasswordResetWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using DAL;

    /// <summary>
    /// View Model for the Password Reset window.
    /// </summary>
    internal class PasswordResetWindowViewModel : Bindable
    {
        private EMPLOYEE employee;

        /// <summary>
        /// Gets or sets the employee whose password we are resetting.
        /// </summary>
        public EMPLOYEE Employee
        {
            get
            {
                return this.employee;
            }

            set
            {
                this.SetProperty(ref this.employee, value);
            }
        }
    }
}
