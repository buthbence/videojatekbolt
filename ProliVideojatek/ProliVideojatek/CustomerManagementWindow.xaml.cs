﻿// <copyright file="CustomerManagementWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for CustomerManagement.xaml
    /// </summary>
    public partial class CustomerManagementWindow : Window
    {
        private CustomerManagementWindowViewModel vM;
        private CustomerManagementWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerManagementWindow"/> class.
        /// </summary>
        /// <param name="customer">Selected customer</param>
        public CustomerManagementWindow(CUSTOMER customer)
        {
            this.InitializeComponent();
            this.vM = new CustomerManagementWindowViewModel();
            this.model = new CustomerManagementWindowModel();

            this.model.Cid = customer.CID;
            this.model.Addr = customer.ADDR;
            this.model.Cname = customer.CNAME;
            this.model.Email = customer.EMAIL;
            this.model.Lcard = (bool)customer.LCARD;
            this.model.Phone = customer.PHONE;
            this.model.CustomerControl = new CustomerControl();
            this.model.SoldItems = this.model.CustomerControl.GetSoldItemsByCustomerId(this.model.Cid);

            this.vM.Cid = this.model.Cid;
            this.vM.Addr = this.model.Addr;
            this.vM.Cname = this.model.Cname;
            this.vM.Email = this.model.Email;
            this.vM.Lcard = this.model.Lcard;
            this.vM.Phone = this.model.Phone;
            this.vM.SoldItems = this.model.SoldItems;

            this.DataContext = this.vM;
        }

        /// <summary>
        /// Called when the user presses Ok button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            CUSTOMER customer = new CUSTOMER()
            {
                CID = this.vM.Cid,
                ACTIVE = true,
                ADDR = this.vM.Addr,
                CNAME = this.vM.Cname,
                EMAIL = this.vM.Email,
                LCARD = this.vM.Lcard,
                PHONE = this.vM.Phone
            };

            this.model.CustomerControl.ChangeCustomerData(customer);
            this.Close();
        }

        /// <summary>
        /// Called when user presses BackButton
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void NameTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Any(x => !char.IsLetter(x)))
            {
                e.Handled = true;
            }
        }

        private void PhoneTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char[] numbers = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+' };
            if (!numbers.Contains(e.Text[0]))
            {
                e.Handled = true;
            }
        }

        private void PhoneTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
