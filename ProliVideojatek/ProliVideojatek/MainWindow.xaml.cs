﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using DAL.Repos;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel vM;
        private MainWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vM = new MainWindowViewModel();
            this.model = new MainWindowModel() { LoginControl = new LoginControl(UniversalRepository.EmployeeRepository) };
            this.DataContext = this.vM;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.model.Username = this.vM.Username;
            this.model.Password = this.pwbox.Password;
            EMPLOYEE user = this.model.LoginControl.Authenticate(this.model.Username, this.model.Password);
            if (user == null)
            {
                MessageBox.Show("Wrong username and/or password. Try again!");
            }
            else
            {
                if (this.model.LoginControl.IsFirstTime(user))
                {
                    new PasswordResetWindow(user).ShowDialog();
                    this.model.Password = string.Empty;
                    this.pwbox.Password = string.Empty;
                }
                else
                {
                    new MainMenuWindow(user).Show();
                    this.Close();
                }
            }
        }

        private void Pwbox_KeyUp(object sender, KeyEventArgs e)
        {
            this.model.Password = this.pwbox.Password;
        }
    }
}
