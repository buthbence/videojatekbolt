﻿// <copyright file="ReservedItemsWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using System.Reflection;
    using Bll;
    using DAL;

    /// <summary>
    /// Model for the reserved items window
    /// </summary>
    internal class ReservedItemsWindowModel
    {
        /// <summary>
        /// Gets or sets the sales control business logic class object
        /// </summary>
        public SalesControl SalesControl { get; set; }

        /// <summary>
        /// Gets a list of all reservations in the database
        /// </summary>
        public List<Reservation> Reservations
        {
            get
            {
                object reservations = this.SalesControl.ReservedItemsList;
                List<Reservation> resList = new List<Reservation>();
                foreach (var item in reservations as IEnumerable<object>)
                {
                    resList.Add(this.GetReservation(item));
                }

                return resList;
            }
        }

        /// <summary>
        /// Gets a list of all reservations in the database filtered by a serach term
        /// </summary>
        /// <returns>List of reservation</returns>
        public List<Reservation> GetReservations()
        {
            List<Reservation> resList = this.Reservations;
            List<Reservation> searchingList = new List<Reservation>();
            foreach (Reservation item in resList)
            {
                searchingList.Add(item);
            }

            return searchingList;
        }

        /// <summary>
        /// Gets the reserved products for one reservation
        /// </summary>
        /// <param name="id">Reservation ID</param>
        /// <returns>Dictionary with the reserved products as keys, and the quantities as values</returns>
        internal Dictionary<PRODUCT, int> GetReservationProductList(decimal id)
        {
            return this.SalesControl.GetReservedProducts(id);
        }

        /// <summary>
        /// Deletes a specified product from a specified reservation
        /// </summary>
        /// <param name="rID">Reservetion id to delete from</param>
        /// <param name="pID">Product is to delete</param>
        /// <returns>True, if there are no other products in the reservation</returns>
        internal bool DeleteProduct(decimal rID, decimal pID)
        {
            return this.SalesControl.DeleteProductFromReservation(rID, pID);
        }

        /// <summary>
        /// Deletes a reservation
        /// </summary>
        /// <param name="rID">Reservation id to delete</param>
        internal void DeleteReservation(decimal rID)
        {
            this.SalesControl.DeleteReservation(rID);
        }

        private Reservation GetReservation(object obj)
        {
            PropertyInfo[] props = obj.GetType().GetProperties();
            Reservation reservation = new Reservation();
            foreach (PropertyInfo prop in props)
            {
                if (prop.Name.Equals("GID"))
                {
                    reservation.GID = (decimal)prop.GetValue(obj);
                }

                if (prop.Name.Equals("CName"))
                {
                    reservation.CName = (string)prop.GetValue(obj);
                }

                if (prop.Name.Equals("TotalPrice"))
                {
                    reservation.TotalPrice = prop.GetValue(obj).ToString();
                }
            }

            return reservation;
        }

        /// <summary>
        /// Inner class to represent one reservation
        /// </summary>
        public class Reservation
        {
            /// <summary>
            /// Gets or sets reservation Id
            /// </summary>
            public decimal GID { get; set; }

            /// <summary>
            /// Gets or sets the customers name who reserved.
            /// </summary>
            public string CName { get; set; }

            /// <summary>
            /// Gets or sets the total price for the reservation
            /// </summary>
            public string TotalPrice { get; set; }
        }
    }
}
