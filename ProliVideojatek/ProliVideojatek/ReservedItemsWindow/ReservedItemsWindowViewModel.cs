﻿// <copyright file="ReservedItemsWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using DAL;

    /// <summary>
    /// Viewmodel class for reserved items window
    /// </summary>
    internal class ReservedItemsWindowViewModel : Bindable
    {
        private Dictionary<PRODUCT, int> reservedProducts;

        private List<ReservedItemsWindowModel.Reservation> reservations;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReservedItemsWindowViewModel"/> class.
        /// </summary>
        public ReservedItemsWindowViewModel()
        {
            this.Reservations = new List<ReservedItemsWindowModel.Reservation>();
        }

        /// <summary>
        /// Gets or sets the reserved products in a dictionary (keys are the products, values are quantities)
        /// </summary>
        public Dictionary<PRODUCT, int> ReservedProducts
        {
            get { return this.reservedProducts; }
            set { this.SetProperty(ref this.reservedProducts, value); }
        }

        /// <summary>
        /// Gets or sets the selected product
        /// </summary>
        public KeyValuePair<PRODUCT, int>? SelectedProduct { get; set; }

        /// <summary>
        /// Gets or sets the reservations list
        /// </summary>
        public List<ReservedItemsWindowModel.Reservation> Reservations
        {
            get { return this.reservations; }
            set { this.SetProperty(ref this.reservations, value); }
        }

        /// <summary>
        /// Gets or sets the selected reservation
        /// </summary>
        public ReservedItemsWindowModel.Reservation SelectedReservation { get; set; }
    }
}
