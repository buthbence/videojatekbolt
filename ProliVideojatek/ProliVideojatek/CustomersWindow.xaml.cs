﻿// <copyright file="CustomersWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using Bll;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for Customers.xaml
    /// </summary>
    public partial class CustomersWindow : Window
    {
        private CustomersWindowModel model;
        private CustomersWindowViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersWindow"/> class.
        /// </summary>
        public CustomersWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Called when user presses Edit button.
        /// </summary>
        /// <param name="sender">The parameter is not used</param>
        /// <param name="e">This parameter is not used</param>
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.SelectedCustomer != null)
            {
                this.model.SelectedCustomer = this.vM.SelectedCustomer;

                if (new CustomerManagementWindow(this.vM.SelectedCustomer).ShowDialog() != null)
                {
                    this.model.Customers = this.model.CustomerControl.ActiveCustomers;
                    this.vM.Customers = this.model.Customers;
                    this.model.CustomerSearchTerm = "Search here...";
                    this.vM.CustomerSearchTerm = this.model.CustomerSearchTerm;
                }
            }
        }

        /// <summary>
        /// Called when user presses Back button.
        /// </summary>
        /// <param name="sender">The parameter is not used</param>
        /// <param name="e">This parameter is not used</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Called when the window is loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used</param>
        /// <param name="e">This parameter is not used</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vM = new CustomersWindowViewModel();
            this.model = new CustomersWindowModel();
            this.model.CustomerControl = new CustomerControl();
            this.model.Customers = this.model.CustomerControl.ActiveCustomers;
            this.vM.Customers = this.model.Customers;
            this.model.CustomerSearchTerm = "Search here...";
            this.vM.CustomerSearchTerm = this.model.CustomerSearchTerm;
            this.DataContext = this.vM;
        }

        /// <summary>
        /// Called when user presses Remove button.
        /// </summary>
        /// <param name="sender">The parameter is not used</param>
        /// <param name="e">This parameter is not used</param>
        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.SelectedCustomer != null)
            {
                if (MessageBox.Show("Are you sure you want to remove the selected customer?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.model.CustomerControl.RemoveCustomer(this.vM.SelectedCustomer);
                    this.model.SelectedCustomer = this.vM.SelectedCustomer;
                    this.model.Customers = this.model.CustomerControl.ActiveCustomers;
                    this.vM.Customers = this.model.Customers;
                }
            }
        }

        /// <summary>
        /// Called when user double clicks in CustomerList.
        /// </summary>
        /// <param name="sender">The parameter is not used</param>
        /// <param name="e">This parameter is not used</param>
        private void CustomerList_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.vM.SelectedCustomer != null)
            {
                this.model.SelectedCustomer = this.vM.SelectedCustomer;

                if (new CustomerManagementWindow(this.vM.SelectedCustomer).ShowDialog() != null)
                {
                    this.model.Customers = this.model.CustomerControl.ActiveCustomers;
                    this.vM.Customers = this.model.Customers;
                }
            }
        }

        /// <summary>
        /// Called when user presses a key inside searchbox.
        /// </summary>
        /// <param name="sender">This parameter is not used</param>
        /// <param name="e">The parameter is not used</param>
        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            this.model.CustomerSearchTerm = this.vM.CustomerSearchTerm;
            this.model.Customers = this.model.CustomerControl.FilterCustomers(this.model.CustomerSearchTerm);
            this.vM.Customers = this.model.Customers;
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
