﻿// <copyright file="NewEmployeeWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using Bll;

    /// <summary>
    /// Model for the employee creator window.
    /// </summary>
    internal class NewEmployeeWindowModel
    {
        /// <summary>
        /// Gets or sets the BLL object used by the window.
        /// </summary>
        public EmployeeControl EmployeeControl { get; set; }

        /// <summary>
        /// Gets or sets the user's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the user's id.
        /// </summary>
        public decimal Id { get; set; }

        /// <summary>
        /// Gets or sets the user's username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the user's password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user is going to be an admin.
        /// </summary>
        public bool IsAdmin { get; set; }
    }
}
