﻿// <copyright file="NewEmployeeWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using Bll;

    /// <summary>
    /// View Model for the employee creator window.
    /// </summary>
    internal class NewEmployeeWindowViewModel : Bindable
    {
        private string name;
        private decimal id;
        private string password;
        private bool isAdmin;

        /// <summary>
        /// Gets or sets the user's name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.SetProperty(ref this.name, value);
                this.Username = EmployeeControl.CreateUsernameFromName(this.name);
                this.OnPropertyChanged("Username");
            }
        }

        /// <summary>
        /// Gets the user's username.
        /// </summary>
        public string Username { get; private set; }

        /// <summary>
        /// Gets or sets the user's id.
        /// </summary>
        public decimal Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.SetProperty(ref this.id, value);
            }
        }

        /// <summary>
        /// Gets or sets the user's password.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.SetProperty(ref this.password, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user is going to be an admin.
        /// </summary>
        public bool IsAdmin
        {
            get
            {
                return this.isAdmin;
            }

            set
            {
                this.SetProperty(ref this.isAdmin, value);
            }
        }
    }
}
