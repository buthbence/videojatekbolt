﻿// <copyright file="EditEmployeeWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using DAL.Repos;
    using WPFlayer;

    /// <summary>
    /// The window that lets you edit employees.
    /// </summary>
    public partial class EditEmployeeWindow : Window
    {
        private EditEmployeeWindowModel model;
        private EditEmployeeWindowViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditEmployeeWindow"/> class.
        /// </summary>
        /// <param name="selectedEmployee">The employee to be edited.</param>
        public EditEmployeeWindow(EMPLOYEE selectedEmployee)
        {
            this.InitializeComponent();
            this.model = new EditEmployeeWindowModel()
            {
                Id = selectedEmployee.EID,
                Name = selectedEmployee.ENAME,
                Username = selectedEmployee.UNAME,
                OldName = selectedEmployee.ENAME,
                OldUsername = selectedEmployee.UNAME,
                IsAdmin = selectedEmployee.BOSS.Value,
                EmployeeControl = new EmployeeControl(UniversalRepository.EmployeeRepository)
            };

            this.vM = new EditEmployeeWindowViewModel()
            {
                Id = selectedEmployee.EID,
                Name = selectedEmployee.ENAME,
                Username = selectedEmployee.UNAME,
                IsAdmin = selectedEmployee.BOSS.Value
            };
        }

        /// <summary>
        /// This method is called when the window has loaded.
        /// </summary>
        /// <param name="sender">The object that caused the event to happen.</param>
        /// <param name="e">Contains arguments for the event.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.vM;
        }

        /// <summary>
        /// This method is called when the user presses the Save button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.Name == string.Empty || this.vM.Name == null)
            {
                MessageBox.Show("Please enter a proper name!");
            }
            else
            {
                this.model.Name = this.vM.Name;
                this.model.Username = this.vM.Username;
                bool hasNameChanged = this.model.OldUsername != this.model.Username || this.model.OldName != this.model.Name;
                this.model.Password = this.vM.Password;
                this.model.IsAdmin = this.vM.IsAdmin;

                if (!hasNameChanged || this.model.EmployeeControl.IsNewUser(this.model.Username))
                {
                    this.model.EmployeeControl.EditEmployee(this.model.Id, this.model.Name, this.model.Username, this.model.Password, this.model.PassReset, this.model.IsAdmin);
                    this.DialogResult = true;
                    this.Close();
                }
                else
                {
                    string newUsername = this.model.EmployeeControl.IncrementUsername(this.model.Username);
                    if (this.PreventUsernameCollision(this.model.Username, newUsername))
                    {
                        this.model.Username = newUsername;
                        this.model.EmployeeControl.EditEmployee(this.model.Id, this.model.Name, this.model.Username, this.model.Password, this.model.PassReset, this.model.IsAdmin);
                        this.DialogResult = true;
                        this.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Asks the user whether they want to use the built in username duplication prevention method.
        /// </summary>
        /// <param name="username">The username they wanted to use.</param>
        /// <param name="newUsername">The username they would be able to use instead.</param>
        /// <returns>A bool value indicating whether the user clicked Yes or No.</returns>
        private bool PreventUsernameCollision(string username, string newUsername)
        {
            MessageBoxResult result = MessageBox.Show(
                    "'" + username + "' already exists in database. " +
                    "Would you like to rename user to '" + newUsername + "' instead?", "Username already exists!",
                    MessageBoxButton.YesNo);
            return result == MessageBoxResult.Yes;
        }

        /// <summary>
        /// Called when the user clicks the Back Button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Called when the user checks the "Password Reset" checkbox.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.></param>
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.model.Password = this.model.EmployeeControl.GeneratePassword();
            this.model.PassReset = true;
            this.vM.Password = this.model.Password;
        }

        /// <summary>
        /// Called when the user unchecks the "Password Reset" checkbox.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.model.Password = string.Empty;
            this.model.PassReset = false;
            this.vM.Password = this.model.Password;
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
