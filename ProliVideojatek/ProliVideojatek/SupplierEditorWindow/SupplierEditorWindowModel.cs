﻿// <copyright file="SupplierEditorWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using DAL;

    /// <summary>
    /// Model for Spupplier Editor Window
    /// </summary>
    internal class SupplierEditorWindowModel
    {
        /// <summary>
        /// Gets or sets the supplier to edit.
        /// </summary>
        public SUPPLIER Supplier { get; set; }
    }
}
