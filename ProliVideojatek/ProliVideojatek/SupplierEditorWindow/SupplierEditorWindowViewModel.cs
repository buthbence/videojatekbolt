﻿// <copyright file="SupplierEditorWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    /// <summary>
    /// ViewModel for the supplier editor window
    /// </summary>
    internal class SupplierEditorWindowViewModel
    {
        /// <summary>
        /// Gets or sets the suppliers Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the suppliers name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the suppliers e-mail address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the suppliers phone numer.
        /// </summary>
        public string Phone { get; set; }
    }
}
