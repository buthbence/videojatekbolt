﻿// <copyright file="MainMenuWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenuWindow : Window
    {
        private MainMenuWindowViewModel vM;
        private EMPLOYEE user;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuWindow"/> class.
        /// </summary>
        /// <param name="user">The currently logged in user.</param>
        public MainMenuWindow(EMPLOYEE user)
        {
            this.InitializeComponent();
            WindowCloser.AutoLogoutControl.ResetTimerStartThread();
            this.user = user;
        }

        /// <summary>
        /// Called when the window is loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vM = new MainMenuWindowViewModel(this.user);
            this.DataContext = this.vM;
        }

        /// <summary>
        /// Called when the user presses the logout button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            this.Close();
        }

        /// <summary>
        /// Called when the user presses the admin page button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void AdminPageButton_Click(object sender, RoutedEventArgs e)
        {
            new AdminPageWindow(this.user).ShowDialog();
        }

        /// <summary>
        /// Called when the user presses the reserved items button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void ReservedItems_Click(object sender, RoutedEventArgs e)
        {
            new ReservedItemsWindow().ShowDialog();
        }

        /// <summary>
        /// Called when the user presses the customers button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void CustomersButton_Click(object sender, RoutedEventArgs e)
        {
            new CustomersWindow().ShowDialog();
        }

        /// <summary>
        /// Called when the user presses the stock management button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void StockManagementButton_Click(object sender, RoutedEventArgs e)
        {
            new StockManagementWindow().ShowDialog();
        }

        /// <summary>
        /// Called when the user presses the product list button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void ProductListButton_Click(object sender, RoutedEventArgs e)
        {
            ProductMenuWindow productMenuWindow = new ProductMenuWindow(this.user);
            productMenuWindow.ShowDialog();
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
