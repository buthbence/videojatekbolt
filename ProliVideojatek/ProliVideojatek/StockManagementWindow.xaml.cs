﻿// <copyright file="StockManagementWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for StockManagement.xaml
    /// </summary>
    public partial class StockManagementWindow : Window
    {
        private StockManagementWindowViewModel vM;
        private StockManagementWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="StockManagementWindow"/> class.
        /// </summary>
        public StockManagementWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Called when the window is loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vM = new StockManagementWindowViewModel();
            this.model = new StockManagementWindowModel();
            this.model.RestockList = this.model.RestockControl.GetRestockList(null);
            this.model.ArrivedProducts = new List<RESTOCK>();
            this.vM.RestockList = this.model.RestockList;
            this.DataContext = this.vM;
        }

        /// <summary>
        /// Called when the user checks a checkbox in the list.
        /// </summary>
        /// <param name="sender">The checkbox the user checked.</param>
        /// <param name="e">The parameter is not used.</param>
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.model.ArrivedProducts.Add((sender as CheckBox).Tag as RESTOCK);
        }

        /// <summary>
        /// Called when the user unchecks a checkbox in the list.
        /// </summary>
        /// <param name="sender">The checkbox the user unchecked.</param>
        /// <param name="e">The parameter is not used.</param>
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.model.ArrivedProducts.Remove((sender as CheckBox).Tag as RESTOCK);
        }

        /// <summary>
        /// Called when the user types something into the searchbox. Filters the results.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            this.model.SearchTerm = this.vM.SearchTerm;
            this.model.RestockList = this.model.RestockControl.GetRestockList(this.model.SearchTerm);
            this.vM.RestockList = this.model.RestockList;
        }

        /// <summary>
        /// Called when the user exits the window without saving changes.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Called when the user saves the changes.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.model.ArrivedProducts.Count != 0)
            {
                if (MessageBox.Show("Are you sure you want to save the changes? This cannot be undone.", "Save Changes", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.model.RestockControl.SaveRestockChanges(this.model.ArrivedProducts);
                    this.Close();
                }
            }
            else
            {
                this.Close();
            }
        }

        /// <summary>
        /// Called when the user wants to remove an item.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete the restock request? This cannot be undone.", "Save Changes", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                this.model.ArrivedProducts.Clear();
                this.model.Selected = this.vM.Selected;
                this.model.RestockControl.DeleteRestockRequest(this.model.Selected);
                this.SearchBox_KeyUp(null, null);
            }
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
           WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
           WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
