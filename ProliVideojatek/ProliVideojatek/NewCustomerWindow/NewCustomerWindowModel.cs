﻿// <copyright file="NewCustomerWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    /// <summary>
    /// Model class for new customer window
    /// </summary>
    internal class NewCustomerWindowModel
    {
        /// <summary>
        /// Gets or sets the new customer field
        /// </summary>
        public DAL.CUSTOMER NewCustomer { get; set; }

        /// <summary>
        /// Gets or sets the customer control BLL class object
        /// </summary>
        public Bll.CustomerControl CustomerControl { get; set; }
    }
}
