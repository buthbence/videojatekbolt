﻿// <copyright file="NewCustomerWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    /// <summary>
    /// Viewmodel for new customer window
    /// </summary>
    internal class NewCustomerWindowViewModel : Bindable
    {
        private string name;
        private string address;
        private string email;
        private string phone;
        private bool loyalty;

        /// <summary>
        /// Gets or sets a value indicating whether the customer is a loyal customer
        /// </summary>
        public bool Loyalty
        {
            get { return this.loyalty; }
            set { this.SetProperty(ref this.loyalty, value); }
        }

        /// <summary>
        /// Gets or sets the phone number field, and calls OnPropertyChanged.
        /// </summary>
        public string Phone
        {
            get { return this.phone; }
            set { this.SetProperty(ref this.phone, value); }
        }

        /// <summary>
        /// Gets or sets the e-mail address field, and calls OnPropertyChanged.
        /// </summary>
        public string Email
        {
            get { return this.email; }
            set { this.SetProperty(ref this.email, value); }
        }

        /// <summary>
        /// Gets or sets the address, and calls OnPropertyChanged.
        /// </summary>
        public string Address
        {
            get { return this.address; }
            set { this.SetProperty(ref this.address, value); }
        }

        /// <summary>
        /// Gets or sets the customers name, and calls OnPropertyChanged.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.SetProperty(ref this.name, value); }
        }
    }
}
