﻿// <copyright file="PurchaseWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for PurchaseWindow.xaml
    /// </summary>
    public partial class PurchaseWindow : Window
    {
        private PurchaseWindowModel model;
        private PurchaseWindowViewModel viewModel;
        private SalesControl salesControl;
        private CustomerControl customerControl;

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseWindow"/> class.
        /// </summary>
        public PurchaseWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseWindow"/> class.
        /// </summary>
        /// <param name="cart">The cart to sell</param>
        /// <param name="user">The employee who sells</param>
        public PurchaseWindow(Dictionary<PRODUCT, int> cart, EMPLOYEE user)
            : this()
        {
            this.salesControl = new SalesControl();
            this.customerControl = new CustomerControl();
            this.model = new PurchaseWindowModel()
            {
                Cart = cart,
                Customers = this.customerControl.ActiveCustomers,
                SelectedCustomer = null,
                IsReservation = false,
                TotalPrice = this.salesControl.CartTotalPrice(cart)
            };

            this.model.User = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.viewModel = new PurchaseWindowViewModel();
            this.viewModel.Cart = this.model.Cart;
            this.viewModel.Customers = new ObservableCollection<CUSTOMER>(this.model.Customers);
            this.viewModel.IsReservation = this.model.IsReservation;
            this.viewModel.TotalPrice = this.model.TotalPrice.ToString();
            this.DataContext = this.viewModel;
        }

        private void NewCustomer_Click(object sender, RoutedEventArgs e)
        {
            CUSTOMER newCustomer = new CUSTOMER();
            NewCustomerWindow newCustomerWindow = new NewCustomerWindow(newCustomer);
            newCustomerWindow.ShowDialog();
            if (newCustomerWindow.DialogResult.HasValue ? newCustomerWindow.DialogResult.Value : false)
            {
                this.model.Customers.Add(newCustomer);
                this.viewModel.Customers.Add(newCustomer);
                this.viewModel.SelectedCustomer = newCustomer;
            }
        }

        private void ProceedButton_Click(object sender, RoutedEventArgs e)
        {
            this.model.SelectedCustomer = this.viewModel.SelectedCustomer;
            this.model.IsReservation = this.viewModel.IsReservation;
            if (this.model.SelectedCustomer != null)
            {
                if (this.model.User != null)
                {
                    decimal id = this.salesControl.Sell(this.model.Cart, this.model.SelectedCustomer, this.model.User, this.model.IsReservation);
                    if (!this.model.IsReservation)
                    {
                        PrintDialog printDialog = new PrintDialog();
                        if (printDialog.ShowDialog() == true)
                        {
                            printDialog.PrintVisual(new PrintControl().GenerateInvoice(id), "Printing invoice");
                        }
                    }

                    this.DialogResult = true;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Please select a customer!", "No customer selected");
            }
        }

        private void CustomersListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.viewModel.SelectedCustomer != null)
            {
                this.model.SelectedCustomer = this.viewModel.SelectedCustomer;
                this.model.TotalPrice = this.salesControl.CartTotalPrice(this.model.Cart, this.model.SelectedCustomer);
                this.viewModel.TotalPrice = this.model.TotalPrice.ToString();
            }
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
