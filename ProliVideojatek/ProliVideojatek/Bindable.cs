﻿// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A class used to make the use of INotifyPropertyChanged easier.
    /// </summary>
    internal class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// An event called when a property is changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// This method runs when a property is changed.
        /// </summary>
        /// <param name="name">The changed property.</param>
        protected void OnPropertyChanged(string name = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// This method changes the value of "field" and calls the OnPropertyChanged method with CallerMemberName.
        /// </summary>
        /// <typeparam name="T">The type of the field to be changed.</typeparam>
        /// <param name="field">The field to be changed.</param>
        /// <param name="newvalue">The new value of field.</param>
        /// <param name="name">The name of the changed property.</param>
        protected void SetProperty<T>(ref T field, T newvalue, [CallerMemberName] string name = null)
        {
            field = newvalue;
            this.OnPropertyChanged(name);
        }
    }
}
