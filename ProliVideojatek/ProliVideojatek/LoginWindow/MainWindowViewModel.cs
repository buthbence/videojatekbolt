﻿// <copyright file="MainWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    /// <summary>
    /// View Model for the Login window.
    /// </summary>
    internal class MainWindowViewModel : Bindable
    {
        private string username;

        /// <summary>
        /// Gets or sets the username that is currently entered into the text box.
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                this.SetProperty(ref this.username, value);
            }
        }
    }
}
