﻿// <copyright file="MainWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using Bll;

    /// <summary>
    /// Model for the Login window.
    /// </summary>
    internal class MainWindowModel
    {
        /// <summary>
        /// Gets or sets BLL class used for logging in.
        /// </summary>
        public LoginControl LoginControl { get; set; }

        /// <summary>
        /// Gets or sets the username of the user trying to log in.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password of the user trying to log in.
        /// </summary>
        public string Password { get; set; }
    }
}
