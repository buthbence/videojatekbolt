﻿// <copyright file="PasswordResetWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using DAL.Repos;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for PasswordResetWindow.xaml
    /// </summary>
    public partial class PasswordResetWindow : Window
    {
        private PasswordResetWindowViewModel vM;
        private PasswordResetWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordResetWindow"/> class.
        /// </summary>
        /// <param name="user">The user whose password has to be reset.</param>
        public PasswordResetWindow(EMPLOYEE user)
        {
            this.InitializeComponent();
            this.vM = new PasswordResetWindowViewModel();
            this.vM.Employee = user;
            this.model = new PasswordResetWindowModel(user) { LoginControl = new LoginControl(UniversalRepository.EmployeeRepository) };
        }

        /// <summary>
        /// Method called when the window is loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.vM;
        }

        /// <summary>
        /// Method called when the user clicks on the button to save the changes.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.model.Password = this.pwbox.Password;
            this.model.PasswordConfirmation = this.pwboxConfirmation.Password;
            if (this.model.LoginControl.UpdatePassword(this.model.User, this.model.Password, this.model.PasswordConfirmation))
            {
                this.DialogResult = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("The entered passwords did not match! Try again!");
            }
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
