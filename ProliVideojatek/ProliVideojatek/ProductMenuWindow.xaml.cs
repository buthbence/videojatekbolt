﻿// <copyright file="ProductMenuWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for ProductMenu.xaml
    /// </summary>
    public partial class ProductMenuWindow : Window
    {
        private ProductMenuWindowViewModel vM;
        private ProductMenuWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductMenuWindow"/> class.
        /// </summary>
        /// <param name="user">Currently logged in user</param>
        public ProductMenuWindow(EMPLOYEE user)
        {
            this.InitializeComponent();

            this.model = new ProductMenuWindowModel();
            this.model.SalesControl = new SalesControl();
            this.model.User = user;
            this.model.ProductSearchTerm = null;
            this.model.OnlyAvailableItems = false;
            this.model.ProductControl = new ProductControl();
            this.model.SalesControl.FreeUpExpiredReserves();
            this.model.ProductsMinPrice = this.model.ProductControl.MinimumPrice;
            this.model.ProductsMaxPrice = this.model.ProductControl.MaximumPrice;
            this.model.SetMinPrice = this.model.ProductControl.MinimumPrice;
            this.model.SetMaxPrice = this.model.ProductControl.MaximumPrice;
            this.model.Cart = new Dictionary<PRODUCT, int>();
            this.model.AddToCartQuantity = 1;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vM = new ProductMenuWindowViewModel();
            this.vM.Products = this.model.ProductControl.GetProducts(null, int.MinValue, int.MaxValue, false, null, null);
            this.vM.ProductsMinPrice = this.model.ProductsMinPrice;
            this.vM.ProductsMaxPrice = this.model.ProductsMaxPrice;
            this.vM.SetMinPrice = this.model.SetMinPrice;
            this.vM.SetMaxPrice = this.model.SetMaxPrice;
            this.vM.Categories = this.model.ProductControl.Categories;
            this.vM.SelectedCategory = this.vM.Categories.First();
            this.vM.SubCategories = this.model.ProductControl.GetSubcategories(this.vM.SelectedCategory);
            this.vM.SelectedSubCategory = this.vM.SubCategories.First();
            this.vM.IsBoss = this.model.User.BOSS.Value;
            this.vM.Cart = this.model.Cart;
            this.vM.AddToCartQuantity = this.model.AddToCartQuantity.ToString();
            this.DataContext = this.vM;
        }

        private void ProductSearchBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            this.model.ProductSearchTerm = (sender as TextBox).Text;
            this.DoSearch();
        }

        private void DoSearch()
        {
            this.vM.Products = this.model.ProductControl.GetProducts(
                this.model.ProductSearchTerm,
                this.model.SetMinPrice,
                this.model.SetMaxPrice,
                this.model.OnlyAvailableItems,
                this.model.SelectedCategory,
                this.model.SelectedSubCategory);
        }

        private void MinPriceSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.model.SetMinPrice = (int)(sender as Slider).Value;
            this.DoSearch();
        }

        private void MaxPriceSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.model.SetMaxPrice = (int)(sender as Slider).Value;
            this.DoSearch();
        }

        private void SetMinPriceBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            string text = (sender as TextBox).Text;
            this.model.SetMinPrice = text == string.Empty ? 0 : int.Parse(text);
            this.vM.SetMinPrice = this.model.SetMinPrice;
            this.DoSearch();
        }

        private void SetMaxPriceBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            string text = (sender as TextBox).Text;
            this.model.SetMaxPrice = text == string.Empty ? 0 : int.Parse(text);
            this.DoSearch();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char[] numbers = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            if (!numbers.Contains(e.Text[0]))
            {
                e.Handled = true;
            }
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void OnlyAvailableBox_Checked(object sender, RoutedEventArgs e)
        {
            this.model.OnlyAvailableItems = (sender as CheckBox).IsChecked.Value;
            this.DoSearch();
        }

        private void CategoryBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            this.model.SelectedCategory = this.vM.SelectedCategory;
            this.vM.SubCategories = this.model.ProductControl.GetSubcategories(this.model.SelectedCategory);
            this.vM.SelectedSubCategory = this.vM.SubCategories.First();
            this.DoSearch();
        }

        private void SubCategoryBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.model.SelectedSubCategory = this.vM.SelectedSubCategory;
            this.DoSearch();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.SelectedProduct != null)
            {
                this.model.SelectedProduct = this.vM.SelectedProduct;
                new EditProductWindow(this.model.SelectedProduct, true, this.model.User).ShowDialog();
            }

            this.RefreshProductList();
        }

        private void ProductList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.vM.SelectedProduct != null)
            {
                this.model.SelectedProduct = this.vM.SelectedProduct;
                new EditProductWindow(this.model.SelectedProduct, true, this.model.User).ShowDialog();
            }

            this.RefreshProductList();
        }

        private void RefreshProductList()
        {
            this.model.ProductsMaxPrice = this.model.ProductControl.MaximumPrice;
            this.model.ProductsMinPrice = this.model.ProductControl.MinimumPrice;
            this.vM.ProductsMaxPrice = this.model.ProductsMaxPrice;
            this.vM.ProductsMinPrice = this.model.ProductsMinPrice;
            this.model.SetMaxPrice = this.model.ProductsMaxPrice;
            this.model.SetMinPrice = this.model.ProductsMinPrice;
            this.vM.SetMaxPrice = this.model.SetMaxPrice;
            this.vM.SetMinPrice = this.model.SetMinPrice;

            this.DoSearch();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.SelectedProduct != null)
            {
                this.model.SelectedProduct = this.vM.SelectedProduct;
                this.model.ProductControl.RemoveProduct(this.model.SelectedProduct);
                this.RefreshProductList();
            }
        }

        private void AddNewProductButton_Click(object sender, RoutedEventArgs e)
        {
            new EditProductWindow(null, false, this.model.User).ShowDialog();
            this.RefreshProductList();
        }

        private void AddToCart_Click(object sender, RoutedEventArgs e)
        {
            this.model.SelectedProduct = this.vM.SelectedProduct;
            this.model.OftenBoughtWithSelectedProduct = this.vM.OftenBoughtWithSelectedProduct;
            if (this.model.OftenBoughtWithSelectedProduct != null || this.model.SelectedProduct != null)
            {
                PRODUCT productToAdd = this.model.SelectedProduct != null ? this.model.SelectedProduct : this.model.OftenBoughtWithSelectedProduct;
                this.model.AddToCartQuantity = int.Parse(this.vM.AddToCartQuantity);
                if (this.model.AddToCartQuantity > 0)
                {
                    try
                    {
                        Dictionary<PRODUCT, int> cart = this.model.SalesControl.AddToCart(
                            this.model.Cart,
                            productToAdd.PID,
                            this.model.AddToCartQuantity);
                        this.model.Cart = cart;
                        this.vM.Cart = this.model.Cart;
                        this.SetTotalPrices();
                    }
                    catch (ProductAlreadyInCartException ex)
                    {
                        MessageBoxResult result = MessageBox.Show("The selected product is already in the cart. Would you like to change the quantity?", "Product already in cart", MessageBoxButton.YesNo);
                        switch (result)
                        {
                            case MessageBoxResult.Yes:
                                try
                                {
                                    this.model.Cart = this.model.SalesControl.EditQuantity(
                                  this.model.Cart,
                                  ex.ActualProduct,
                                  this.model.AddToCartQuantity);
                                }
                                catch (ProductIsNotInStockException)
                                {
                                    MessageBox.Show("Product is not in stock!");
                                }

                                this.vM.Cart = this.model.Cart;
                                this.SetTotalPrices();
                                break;
                            default:
                                break;
                        }
                    }
                    catch (ProductIsNotInStockException)
                    {
                        MessageBox.Show("Product is not in stock!");
                    }
                }
            }
        }

        private void SetTotalPrices()
        {
            this.model.CalculateTotalPrices();
            this.vM.TotalPrice = string.Format(
                "Total price: {0} Ft, with Loyalty card: {1} Ft",
                this.model.CartTotalPrice.ToString(),
                this.model.CartTotalProceForLoyals.ToString());
        }

        private void Cart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.model.SelectedProductInCartQuantity = this.vM.SelectedCartItem.Value;
            this.vM.SelectedProductInCartQuantity = this.model.SelectedProductInCartQuantity.ToString();
        }

        private void EditCartItemQtyTextBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (this.vM.SelectedCartItem.Key != null)
            {
                this.model.SelectedProductInCart = this.vM.SelectedCartItem.Key;
                int newQty;
                TextBox t = sender as TextBox;
                if (int.TryParse(t.Text == string.Empty ? "0" : t.Text, out newQty) && this.model.SelectedProductInCart.STOCK >= newQty)
                {
                    this.model.SelectedProductInCartQuantity = newQty;
                    this.model.Cart = this.model.SalesControl.EditQuantity(this.vM.Cart, this.model.SelectedProductInCart, this.model.SelectedProductInCartQuantity == 0 ? 1 : this.model.SelectedProductInCartQuantity);
                    this.vM.Cart = this.model.Cart;
                    foreach (KeyValuePair<PRODUCT, int> item in this.vM.Cart)
                    {
                        if (item.Key.Equals(this.model.SelectedProductInCart))
                        {
                            this.vM.SelectedCartItem = item;
                            break;
                        }
                    }
                }
                else
                {
                    this.vM.SelectedProductInCartQuantity = this.model.SelectedProductInCartQuantity.ToString();
                }

                t.CaretIndex = t.Text.Length;
            }
        }

        private void Finish_Click(object sender, RoutedEventArgs e)
        {
            this.model.Cart = this.vM.Cart;
            PurchaseWindow purchaseWindow = new PurchaseWindow(this.model.Cart, this.model.User);
            if (this.model.Cart.Count > 0)
            {
                purchaseWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("Cart is empty!");
            }

            if (purchaseWindow.DialogResult.HasValue ? purchaseWindow.DialogResult.Value : false)
            {
                this.Close();
            }
        }

        private void ProductListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.model.SelectedProduct = this.vM.SelectedProduct;
            if (this.model.SelectedProduct != null)
            {
                this.model.OftenBoughtWithSelectedProduct = null;
                this.vM.OftenBoughtWithSelectedProduct = this.model.OftenBoughtWithSelectedProduct;
                this.model.OftenBoughtWith = this.model.SalesControl.GetOftenBoughtTogetherProducts(this.model.SelectedProduct);
                this.vM.OftenBoughtWith = this.model.OftenBoughtWith;
            }
        }

        private void OftenBoughtWithListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.model.OftenBoughtWithSelectedProduct = this.vM.OftenBoughtWithSelectedProduct;
            if (this.model.OftenBoughtWithSelectedProduct != null)
            {
                this.model.SelectedProduct = null;
                this.vM.SelectedProduct = this.model.SelectedProduct;
            }
        }

        private void RemoveFromCartButton_Click(object sender, RoutedEventArgs e)
        {
            this.model.SelectedProductInCart = this.vM.SelectedCartItem.Key;
            this.model.SelectedProductInCartQuantity = this.vM.SelectedCartItem.Value;
            if (this.model.SelectedProductInCart != null)
            {
                Dictionary<PRODUCT, int> newCart = new Dictionary<PRODUCT, int>(this.model.Cart);
                newCart.Remove(this.model.SelectedProductInCart);
                this.model.SelectedProductInCart = newCart.FirstOrDefault().Key;
                this.vM.SelectedCartItem = newCart.FirstOrDefault();
                this.model.Cart = newCart;
                this.vM.Cart = this.model.Cart;
            }
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
