//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Product class.
    /// </summary>
    public partial class PRODUCT
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PRODUCT"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCT()
        {
            this.SOLDITEM = new HashSet<SOLDITEM>();
            this.SUPPROD = new HashSet<SUPPROD>();
            this.RESTOCK = new HashSet<RESTOCK>();
        }
        
        /// <summary>
        /// Gets or sets a product's id.
        /// </summary>
        public decimal PID { get; set; }

        /// <summary>
        /// Gets or sets a product's name.
        /// </summary>
        public string PNAME { get; set; }

        /// <summary>
        /// Gets or sets a product's subcategory id.
        /// </summary>
        public decimal PSUBCAT { get; set; }

        /// <summary>
        /// Gets or sets a product's stock value.
        /// </summary>
        public Nullable<decimal> STOCK { get; set; }

        /// <summary>
        /// Gets or sets a product's price.
        /// </summary>
        public Nullable<decimal> PRICE { get; set; }

        /// <summary>
        /// Gets or sets a product's active status.
        /// </summary>
        public bool ACTIVE { get; set; }

        /// <summary>
        /// Gets or sets a product's discount value.
        /// </summary>
        public Nullable<decimal> DISCOUNT { get; set; }

        /// <summary>
        /// Gets or sets a solditem collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SOLDITEM> SOLDITEM { get; set; }

        /// <summary>
        /// Gets or sets a supprod collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SUPPROD> SUPPROD { get; set; }

        /// <summary>
        /// Gets or sets a restock collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RESTOCK> RESTOCK { get; set; }

        /// <summary>
        /// Gets or sets a product's subcategory.
        /// </summary>
        public virtual SUB_CATEGORY SUB_CATEGORY { get; set; }
    }
}
