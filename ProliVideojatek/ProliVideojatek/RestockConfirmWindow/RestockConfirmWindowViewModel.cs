﻿// <copyright file="RestockConfirmWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using DAL;

    /// <summary>
    /// ViewModel for the restock confirmation window.
    /// </summary>
    public class RestockConfirmWindowViewModel
    {
        /// <summary>
        /// Gets or sets the supplier that will restock the product.
        /// </summary>
        public SUPPLIER Supplier { get; set; }

        /// <summary>
        /// Gets or sets the product that the user wants to restock.
        /// </summary>
        public PRODUCT Product { get; set; }

        /// <summary>
        /// Gets or sets the amount the user wants to order from the supplier.
        /// </summary>
        public int RestockAmount { get; set; }
    }
}
