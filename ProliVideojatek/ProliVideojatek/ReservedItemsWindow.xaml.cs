﻿// <copyright file="ReservedItemsWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Bll;
    using DAL;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for ReservedItemsWindow.xaml
    /// </summary>
    public partial class ReservedItemsWindow : Window
    {
        private ReservedItemsWindowViewModel viewModel;
        private ReservedItemsWindowModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReservedItemsWindow"/> class.
        /// </summary>
        public ReservedItemsWindow()
        {
            this.InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.viewModel = new ReservedItemsWindowViewModel();
            this.model = new ReservedItemsWindowModel();
            this.model.SalesControl = new SalesControl();
            this.model.SalesControl.FreeUpExpiredReserves();
            this.viewModel.Reservations = this.model.Reservations;

            this.DataContext = this.viewModel;
        }

        private void ReservationsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                this.viewModel.ReservedProducts = this.model.GetReservationProductList(this.viewModel.SelectedReservation.GID);
            }
            catch (NullReferenceException)
            {
                this.viewModel.ReservedProducts = null;
            }
        }

        private void Proceed_Click(object sender, RoutedEventArgs e)
        {
            if (this.viewModel.SelectedReservation != null)
            {
                int price = this.model.SalesControl.CartTotalPrice(this.viewModel.ReservedProducts);
                MessageBoxResult result = MessageBox.Show("Total price: " + price + " Ft\nSell selected reservation?", "Sell reserved products", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    this.model.SalesControl.SellReservation(this.viewModel.SelectedReservation.GID);

                    PrintDialog printDialog = new PrintDialog();
                    if (printDialog.ShowDialog() == true)
                    {
                        printDialog.PrintVisual(new PrintControl().GenerateInvoice(this.viewModel.SelectedReservation.GID), "Printing invoice");
                    }

                    this.viewModel.Reservations = this.model.Reservations;
                    this.viewModel.SelectedReservation = null;
                }
            }
        }

        private void RemoveOrder_Click(object sender, RoutedEventArgs e)
        {
            if (this.viewModel.SelectedReservation != null)
            {
                this.model.DeleteReservation(this.viewModel.SelectedReservation.GID);
                this.viewModel.Reservations = this.model.GetReservations();
            }
        }

        private void RemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            if (this.viewModel.SelectedProduct != null && !this.viewModel.SelectedProduct.Equals(default(KeyValuePair<PRODUCT, int>)))
            {
                bool isReservationEmpty = this.model.DeleteProduct(this.viewModel.SelectedReservation.GID, this.viewModel.SelectedProduct.Value.Key.PID);
                this.viewModel.SelectedProduct = null;
                this.viewModel.ReservedProducts = this.model.GetReservationProductList(this.viewModel.SelectedReservation.GID);
                if (isReservationEmpty)
                {
                    this.model.DeleteReservation(this.viewModel.SelectedReservation.GID);
                    this.viewModel.SelectedReservation = null;
                    this.viewModel.Reservations = this.model.GetReservations();
                }
            }
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
