﻿// <copyright file="CustomerManagementWindowModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using Bll;
    using DAL;

    /// <summary>
    /// Model of CustomerManagementWindow
    /// </summary>
    internal class CustomerManagementWindowModel
    {
        /// <summary>
        /// Gets or sets Customer id.
        /// </summary>
        public decimal Cid { get; set; }

        /// <summary>
        /// Gets or sets Customer name.
        /// </summary>
        public string Cname { get; set; }

        /// <summary>
        /// Gets or sets Customer Addr.
        /// </summary>
        public string Addr { get; set; }

        /// <summary>
        /// Gets or sets Email of customer.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Phone number of customer,
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer have a localty card.
        /// </summary>
        public bool Lcard { get; set; }

        /// <summary>
        /// Gets or sets the items the customer bought.
        /// </summary>
        public List<SOLDITEM> SoldItems { get; set; }

        /// <summary>
        /// Gets or sets the BLL object for the Customer Management Window.
        /// </summary>
        public CustomerControl CustomerControl { get; set; }
    }
}
