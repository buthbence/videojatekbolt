﻿// <copyright file="CustomerManagementWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Collections.Generic;
    using DAL;

    /// <summary>
    /// View model for CustomerManagementWindow.
    /// </summary>
    internal class CustomerManagementWindowViewModel
    {
        /// <summary>
        /// Gets or sets Customer id.
        /// </summary>
        public decimal Cid { get; set; }

        /// <summary>
        /// Gets or sets Customer name,
        /// </summary>
        public string Cname { get; set; }

        /// <summary>
        /// Gets or sets Customer address.
        /// </summary>
        public string Addr { get; set; }

        /// <summary>
        /// Gets or sets Customer email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Customer phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer has a Loyalty card.
        /// </summary>
        public bool Lcard { get; set; }

        /// <summary>
        /// Gets or sets items the customer bought.
        /// </summary>
        public List<SOLDITEM> SoldItems { get; set; }
    }
}
