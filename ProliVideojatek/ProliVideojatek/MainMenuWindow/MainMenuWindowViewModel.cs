﻿// <copyright file="MainMenuWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using DAL;

    /// <summary>
    /// ViewModel for the Main Menu window.
    /// </summary>
    internal class MainMenuWindowViewModel : Bindable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuWindowViewModel"/> class.
        /// </summary>
        /// <param name="user">The currently logged in user.</param>
        public MainMenuWindowViewModel(EMPLOYEE user)
        {
            this.Employee = user;
        }

        /// <summary>
        /// Gets the currently logged in user.
        /// </summary>
        public EMPLOYEE Employee { get; private set; }
    }
}
