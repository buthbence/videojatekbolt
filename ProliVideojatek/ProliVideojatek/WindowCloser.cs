﻿// <copyright file="WindowCloser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WPFlayer
{
    using System.Windows;
    using Bll;
    using ProliVideojatek;

    /// <summary>
    /// Class that can close all windows in the application.
    /// </summary>
    internal class WindowCloser
    {
        private static AutoLogoutControl autoLogoutControl = new AutoLogoutControl(CloseWindowsOpenLogin);

        /// <summary>
        /// Gets the BLL of the automatic logout system.
        /// </summary>
        public static AutoLogoutControl AutoLogoutControl
        {
            get
            {
                return autoLogoutControl;
            }
        }

        /// <summary>
        /// Closes all the windows in the application and opens the login window.
        /// </summary>
        private static void CloseWindowsOpenLogin()
        {
            MessageBox.Show("Time expired. Please login again.");
            MainWindow login = new MainWindow();
            foreach (Window window in Application.Current.Windows)
            {
                if (Application.Current.Windows.Count == 1)
                {
                    login.Show();
                }

                if (window != login)
                {
                    window.Close();
                }
            }
        }
    }
}
