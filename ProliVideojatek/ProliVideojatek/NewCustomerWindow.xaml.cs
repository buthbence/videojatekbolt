﻿// <copyright file="NewCustomerWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProliVideojatek
{
    using System.Windows;
    using System.Windows.Input;
    using WPFlayer;

    /// <summary>
    /// Interaction logic for NewCustomerWindow.xaml
    /// </summary>
    public partial class NewCustomerWindow : Window
    {
        private NewCustomerWindowModel model;
        private NewCustomerWindowViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewCustomerWindow"/> class.
        /// </summary>
        public NewCustomerWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NewCustomerWindow"/> class.
        /// </summary>
        /// <param name="newCustomer">New Customer</param>
        public NewCustomerWindow(DAL.CUSTOMER newCustomer)
            : this()
        {
            this.model = new NewCustomerWindowModel()
            {
                CustomerControl = new Bll.CustomerControl(),
                NewCustomer = newCustomer
            };
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.model == null)
            {
                this.model = new NewCustomerWindowModel()
                {
                    CustomerControl = new Bll.CustomerControl(),
                    NewCustomer = new DAL.CUSTOMER()
                };
            }

            this.viewModel = new NewCustomerWindowViewModel();
            this.DataContext = this.viewModel;
        }

        private void AddCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            this.model.NewCustomer.CNAME = this.viewModel.Name;
            this.model.NewCustomer.ADDR = this.viewModel.Address;
            this.model.NewCustomer.EMAIL = this.viewModel.Email;
            this.model.NewCustomer.PHONE = this.viewModel.Phone;
            this.model.NewCustomer.LCARD = this.viewModel.Loyalty;
            if (string.IsNullOrEmpty(this.model.NewCustomer.CNAME) || string.IsNullOrWhiteSpace(this.model.NewCustomer.CNAME))
            {
                MessageBox.Show("Please enter a name!");
            }
            else if (string.IsNullOrEmpty(this.model.NewCustomer.ADDR) || string.IsNullOrWhiteSpace(this.model.NewCustomer.ADDR))
            {
                MessageBox.Show("Please enter an address!");
            }
            else if (string.IsNullOrEmpty(this.model.NewCustomer.EMAIL) || string.IsNullOrWhiteSpace(this.model.NewCustomer.EMAIL))
            {
                MessageBox.Show("Please enter an email!");
            }
            else if (string.IsNullOrEmpty(this.model.NewCustomer.PHONE) || string.IsNullOrWhiteSpace(this.model.NewCustomer.PHONE))
            {
                MessageBox.Show("Please enter a phone number!");
            }
            else
            {
                this.model.CustomerControl.AddCustomer(this.model.NewCustomer);
                this.DialogResult = true;
                this.Close();
            }
        }

        /// <summary>
        /// Called when the user moves the mouse. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }

        /// <summary>
        /// Called when the user presses a key. Used to reset logout timer.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            WindowCloser.AutoLogoutControl.ResetTimer();
        }
    }
}
